describe('Controllers', function(){
    var scope;

    // load the controller's module
    beforeEach(module('starter'));

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();
        $controller('CommunityController', {$scope: scope});
    }));

    // tests start here
    it('should have enabled friends to be true', function(){
        expect(scope.foo).toEqual("bar");
    });
});