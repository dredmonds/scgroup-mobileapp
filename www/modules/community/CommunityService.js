angular.module('starter.services')

.service('CommunityService', function( $http, $q, $rootScope) {

	// Return public API.
	return({
	    getCommunities: getCommunities,
	    getPosts: getPosts,
	    getPost: getPost,
	    addPost: addPost,
	    updatePost: updatePost,
	    deletePost: deletePost,
	    addLike: addLike,
	    deleteLike: deleteLike,
	    getLikes:getLikes,
	    addComment: addComment,
	    updateComment: updateComment,
	    deleteComment: deleteComment,
	    getComments:getComments,
	    addSubscription:addSubscription,
	    deleteSubscription:deleteSubscription,
	});

	function getCommunities(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "GET",
	        url: host+"communities/"+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function getPosts(id, page, restore, query, person) {
		var host = localStorage["serverURL"];

		if(!restore){
			var url = host+"posts/"+id+"?page="+page;
		} else {
			var url = host+"posts/"+id+"?amount="+restore;
		}

	    var request = $http({
	        method: "GET",
	        url: url,
	        params: {
	        	search:query,
	        	user:person
	        },
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function getPost(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "GET",
	        url: host+"posts/0?post="+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addPost(data) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "post",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"posts",
	        params: {},
	        data: data
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updatePost(data) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "put",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"posts",
	        params: {},
	        data: data
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function deletePost(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "delete",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"posts/"+id,
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addLike(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "post",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"likes",
	        params: {},
	        data: {post:id}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}


	function deleteLike(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "delete",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"likes/"+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function getLikes(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "get",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"likes/"+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addComment(id, body) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "post",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"comments",
	        params: {},
	        data: {post:id, body:body, date:new Date()}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}


	function updateComment(comment) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "put",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"comments/",
	        data: {_id:comment._id, body:comment.body}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}


	function deleteComment(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "delete",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"comments/"+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function getComments(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "get",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"comments/"+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function addSubscription(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "post",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"subscribe",
	        params: {},
	        data: {post:id}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}


	function deleteSubscription(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "delete",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: host+"subscribe/"+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}


	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});
