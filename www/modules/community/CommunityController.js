angular.module('starter.controllers')

.controller('CommunityController', function($scope, $ionicHistory, $ionicPopup, CommunityService, $stateParams, $cordovaFileTransfer, $cordovaCamera, $state, $rootScope, $ionicSlideBoxDelegate, $ionicScrollDelegate, $ionicModal, $timeout, $ionicPopover, $ionicLoading, $ionicPlatform) {

  // Getting module options from config file
  for (var i = 0; i < $scope.config.features.length; i++) {
      if($scope.config.features[i].index == $state.params.featureIndex){
          $scope.feature = $scope.config.features[i];
      }
  }

  $ionicPlatform.onHardwareBackButton(function() {
    if($rootScope.isFullScreenMode)
      closeGalleryModal();
  });

  $scope.$on('$ionicView.enter', function(e) {
      if(window.cordova) screen.lockOrientation('portrait');
  });

  var vm = this,
      currentState = $state.current.name;

  vm.clearSearch = clearSearch;
  vm.detailedView = detailedView;
  vm.loadMoreData = loadMoreData;
  vm.refreshFeed = refreshFeed;
  vm.clearFilters = clearFilters;
  vm.selectFilter = selectFilter;
  vm.goSearch = goSearch;
  vm.openFilterPopover = openFilterPopover;
  vm.closeFilterPopover = closeFilterPopover;
  vm.openMorePopover = openMorePopover;
  vm.closeMorePopover = closeMorePopover;
  vm.openPostModal = openPostModal;
  vm.openEditPostModal = openEditPostModal;
  vm.closePostModal = closePostModal;
  vm.removeMedia = removeMedia;
  vm.savePost = savePost;
  vm.deletePost = deletePost;
  vm.likePost = likePost;
  vm.subscribe = subscribe;
  vm.scrollTop = scrollTop;
  // vm.searchFor = searchFor;

  vm.selectMedia = openFilePicker;
  vm.mediaArray = [];
  vm.attachments = [];
  vm.posts = [];
  vm.filtered = {"results":[]};
  vm.phoneHeight = window.innerHeight;
  vm.moreDataCanBeLoaded = true;

  vm.communities;
  vm.writePermission = false;
  vm.comment;
  vm.post;

  vm.openGalleryModal = openGalleryModal;
  vm.closeGalleryModal = closeGalleryModal;
  vm.slideVisible = slideVisible;
  vm.openVideo = openVideo;
  vm.slideChanged = slideChanged;

  if(currentState == 'app.community-detail'){
    vm.openCommentModal = openCommentModal;
    vm.closeCommentModal = closeCommentModal;
    vm.postComment = postComment;
    vm.deleteComment = deleteComment;
    vm.openCommentMorePopover = openCommentMorePopover;
    vm.closeCommentMorePopover = closeCommentMorePopover;

    vm.comment = {body:""}
    vm.post = JSON.parse($stateParams.post);

    getLikes();
    getComments();
  }

  $scope.$on( "$ionicView.beforeEnter", function( scopes, states ) {

    currentState = $state.current.name;

    if(states.direction != 'back' && currentState == 'app.community'){
      $rootScope.pagePagination = 0;
      $scope.searchQuery = $stateParams.search ? $stateParams.search : $scope.searchQuery;
      getCommunities(true, null, $scope.searchQuery);
    } else if(currentState == 'app.community' || currentState == 'app.community-detail'){
      getCommunities(true, true, $scope.searchQuery);
    }

    if($stateParams.person) {
      vm.personPosts = true;
      vm.personName = $stateParams.personName;
      $scope.searchQuery = null;
    } else {
      vm.personPosts = false;
    }

    vm.getTimeStamp = Date.now();

  });

  // Imgix parameters
  $scope.imgixParameters = '?auto=format&ch=Save-Data&max-h='+$scope.deviceWidth*1.5*$scope.deviceDPR+'&fit=crop&crop=entropy&min-h='+$scope.deviceWidth/2+'&w='+$scope.deviceWidth;

  $rootScope.$on("refreshComments", function(evt,data){
    if(data == vm.post._id){
      getComments(true);
    }
  });

  function clearSearch (){
    if(vm.searchActivated){
      $scope.searchQuery = '';
      refreshFeed();
    } else {
      $scope.searchQuery = '';
    }
  }

  function detailedView (post){
    var post = JSON.stringify(post);
    $state.go("app.community-detail", {featureIndex:$stateParams.featureIndex, post:post});
  }

  function getCommunities(posts, restore, query){

    if(posts == true){
      $ionicLoading.show();
    }

    if($state.current.name == 'app.community-detail') {
      $ionicLoading.hide();
    }

    CommunityService.getCommunities($scope.feature.moduleId)
      .then(function(success){

        if(success.data && success.data.length > 0){

          // for (var i = success.data.length - 1; i >= 0; i--) {
          //   vm.communitiesObject[success.data[i]._id] = success.data[i];
          // }

          if(posts == true && localStorage["communities"] && localStorage["communities"].length > 1){

            var oldCommunityList = JSON.parse(localStorage["communities"]);
            var newCommunityList = [];

            for (var i = 0; i < success.data.length; i++) {
              for (var x = 0; x < oldCommunityList.length; x++) {
                 if(success.data[i]._id == oldCommunityList[x]._id) {
                    if(oldCommunityList[x].selected == true){
                      success.data[i].selected = true;
                    }
                 }
              }
              newCommunityList.push(success.data[i]);
            }

            vm.communities = newCommunityList;
            localStorage["communities"] = JSON.stringify(newCommunityList);
            getPosts(vm.communities, $rootScope.pagePagination, restore, query);

          } else if(posts == true){

            vm.communities = success.data;
            getPosts(vm.communities, $rootScope.pagePagination, restore, query);
          }
        } else {
          $ionicLoading.hide();
        }
      }, function(error){
        console.log(error);
        $ionicLoading.hide();
      });

  }

  function getPosts(communities, page, restore, query){
    if(restore == true){
        restore = (($rootScope.pagePagination*10)+10);
        console.log("restore to "+restore+" posts.")
        var posts = [];
    } else if(page == 0) {
        $rootScope.pagePagination = 0;
        var posts = [];
    } else {
        var posts = vm.posts;
    }

    var communitiesArray = [];
    var allCommunitiesArray = [];
    var noCommunitiesSelected = true;
    var numberCommunitiesSelected = 0;

    for (var i = communities.length - 1; i >= 0; i--) {
      if(communities[i].myPermission == 3) { vm.writePermission = true; };
      allCommunitiesArray.push(communities[i]._id);
      if(communities[i].selected == true){
        noCommunitiesSelected = false;
        numberCommunitiesSelected++;
        communitiesArray.push(communities[i]._id);
      }

    }

    if(noCommunitiesSelected){
      communitiesArray = allCommunitiesArray;
    }

    vm.filterCount = numberCommunitiesSelected;
    if(vm.filterCount > 0){
      vm.searchCloseMargin = '130px';
    } else if (vm.communities.length > 1){
      vm.searchCloseMargin = '100px';
    }


    CommunityService.getPosts(communitiesArray, page, restore, query, $stateParams.person)
      .then(function(success){
        $timeout(function(){
          if(success.data){
            for (var i = success.data.length - 1; i >= 0; i--) {

              success.data[i].author.full_name = success.data[i].author.first_name + ' ' +success.data[i].author.last_name;

              if(success.data[i].likes.indexOf($rootScope.currentUser._id) > -1 ){
                success.data[i].liked = true;
              }
              if(success.data[i].subscribe.indexOf($rootScope.currentUser._id) > -1 ){
                success.data[i].subscribed = true;
              }
              posts.push(success.data[i]);

            }
            vm.moreDataCanBeLoaded = true;
          } else {
            vm.moreDataCanBeLoaded = false;
          }

          vm.posts = posts;
          console.log("All posts loaded: ", vm.posts);
          $scope.$broadcast('scroll.refreshComplete');
          $scope.$broadcast('scroll.infiniteScrollComplete');
          $ionicLoading.hide();

          if($scope.searchQuery && $scope.searchQuery.length > 0) {
            vm.searchActivated = true;
          } else {
            vm.searchActivated = false;
          }

        },1000);

      }, function(error){

        $ionicLoading.hide();
        console.log(error);

      })

  }

  function loadMoreData () {
    $rootScope.pagePagination++;
    getPosts(vm.communities, $rootScope.pagePagination, null, $scope.searchQuery);
  };

  function refreshFeed (query) {
    $rootScope.pagePagination = 0;
    getCommunities(true, null, $scope.searchQuery);
  }

  function clearFilters () {
    for(x in vm.communities){
      vm.communities[x].selected = false;
    }
    localStorage["communities"] = JSON.stringify(vm.communities);
  }

  function selectFilter (index) {
    if(vm.communities[index].selected){
      vm.communities[index].selected = false;
    } else {
      vm.communities[index].selected = true;
    }
    var selected = 0;
    for (var i = vm.communities.length - 1; i >= 0; i--) {
      if(vm.communities[i].selected == true){
        selected++;
      }
    }
    localStorage["communities"] = JSON.stringify(vm.communities);
  }

  function goSearch (query) {
    refreshFeed(query);
  }

  // .fromTemplateUrl() method
  $ionicPopover.fromTemplateUrl('filter-popover.html', {
    scope: $scope,
    "backdropClickToClose" :false
  }).then(function(popover) {
    vm.filterPopover = popover;
  });

  function openFilterPopover ($event) {
    vm.popoverItems = vm.communities.length + 2;
    var windowHeight = window.innerHeight-120;
    vm.popoverHeight = ((vm.popoverItems-1)*51)+68;
    if(vm.popoverHeight > windowHeight) {
      vm.popoverHeight = windowHeight;
    }
    vm.filterPopover.show($event);

  };
  function closeFilterPopover () {
    vm.posts = [];
    $rootScope.pagePagination = 0;
    $ionicLoading.show();
    getCommunities(true, null, $scope.searchQuery);
    vm.filterPopover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    if(vm.filterPopover) vm.filterPopover.remove();
  });

  // .fromTemplateUrl() method
  $ionicPopover.fromTemplateUrl('more-popover.html', {
    scope: $scope
  }).then(function(popover) {
    vm.morePopover = popover;
  });

  function openMorePopover ($event, post) {
    vm.post = post;
    vm.popoverItems = 1;
    console.log(vm.popoverItems)
    if(vm.post.author._id == $scope.currentUser._id  && vm.post.communityInfo.myPermission == 3 || $scope.currentUser.role == 'admin'){
      vm.popoverItems++;
    }
    if(vm.post.author._id == $scope.currentUser._id && vm.post.communityInfo.myPermission == 3 || $scope.currentUser.role == 'admin'){
      vm.popoverItems++;
    }
    if(vm.post.author._id == $scope.currentUser._id && vm.post.communityInfo.myPermission == 3|| $scope.currentUser.role == 'admin'){
      vm.popoverItems++;
    }
    console.log(vm.popoverItems)
    vm.morePopover.show($event);
  };
  function closeMorePopover () {
    vm.morePopover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    vm.morePopover.remove();
  });

  $ionicModal.fromTemplateUrl('modules/community/_communityPostModal.html', {
    scope: $scope,
    animation: 'slide-in-up',
    backdropClickToClose: false
  }).then(function(modal) {
    vm.postModal = modal;
  });
  function openPostModal () {
    vm.editMode = false;
    vm.mediaArray = [];
    vm.post = {
      title: '',
      content: '',
      status: $rootScope.currentUser.role == 'admin' ? '1':'0',
      date: new Date(),
      community: "",
      attachments: [],
      author:$rootScope.currentUser._id
    };
    vm.postModal.show();
    vm.attachments = [];
    console.log(vm.mediaArray)
    console.log(vm.post)
    console.log(vm.attachments)
  };
  function openEditPostModal () {
    vm.editedPost = angular.copy(vm.post);
    vm.editMode = true;
    vm.morePopover.hide();
    vm.mediaArray = angular.copy(vm.post.attachments);
    vm.attachments = [];
    vm.postModal.show();
  }
  function closePostModal () {
    vm.post.community = vm.post.community._id;
    vm.postModal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    vm.postModal.remove();
  });

  function openFilePicker() {

      var options = {
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
        mediaType: Camera.MediaType.ALLMEDIA,
        correctOrientation: true,
        quality: 75,
        // targetHeight: 100,
        // targetWidth: 100
      };

      $cordovaCamera.getPicture(options).then(function(imageUri) {

          // Do something with image
          var string = imageUri.toLowerCase();

          if(!string.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/)){
            // alert("Video uploads do not work at the moment")
            // return
            vm.mediaArray.push({file:imageUri, type:"video"});
          } else {
            vm.mediaArray.push({file:imageUri, type:"image"});
          }

          vm.$apply();

      }, function(err) {

          console.debug("Unable to obtain picture: " + err, "app");
          alert(error);

      });

  }

  function removeMedia (index) {
    vm.mediaArray.splice(index, 1);
  }

  var moderation = false;
  function savePost () {

    var missingFields = [];
    if (!vm.post || !vm.post.community) {
      missingFields.push("<br>Community");
    }
    if (!vm.post || !vm.post.title) {
      missingFields.push("<br>Title");
    }
    if (!vm.post || !vm.post.content) {
      missingFields.push("<br>Content");
    }
    if(missingFields.length > 0){
        $ionicPopup.alert({
          title: 'Before you post:',
          template: '<center>Please ensure the following fields have been entered correctly:<br>'+missingFields.toString().replace(/,/g , " ")+'</center>'
        });
    } else {

      for (var i = 0; i < vm.communities.length; i++) {
        if(vm.communities[i]._id == vm.post.community){
          moderation = vm.communities[i].moderation;
        }
      }

      $ionicLoading.show({template:"Uploading"});
      if(vm.mediaArray.length > 0) {
        var count = 0;
        var length = vm.mediaArray.length;
        uploadFile();
        function uploadFile () {
          var token = localStorage["token"].replace(/['"]+/g, '');
          if(count < length && vm.mediaArray[count].file){

            var options = {
              data: {
                file: vm.mediaArray[count].file
              },
              headers: {
                token: token,
                code:localStorage["orgId"]
              }
            }
            // Set the mimeType if it a video
            // if(vm.mediaArray[count].type == 'video') {
            //   options.mimeType = "video/quicktime";
            // }
            console.log("this is the file to be uploaded");
            console.log(JSON.stringify(vm.mediaArray[count]));
            $cordovaFileTransfer.upload(localStorage["serverURL"] + 'upload', vm.mediaArray[count].file, options)
              .then(function(result) {
                console.log("this is the result");
                console.log(result.response);
                var data = JSON.parse(result.response)[0];
                vm.attachments.push(data);
                if(count < length-1){
                  count++;
                  uploadFile();
                } else {
                  addPost();
                }
              }, function(error){

                console.log(error);
                $ionicPopup.alert({
                    title: 'Error',
                    template: error.body,
                    okType: 'button-dark'
                });

                $ionicLoading.hide();
              }, function (progress) {
                $ionicLoading.show({template:"Uploading files "+(count+1)+"/"+length+" - "+((progress.loaded / progress.total) * 100).toFixed(0)+"%"});
              });
          } else if(count < length){
            vm.attachments.push(vm.mediaArray[count]);
            count++;
            uploadFile();
          } else {
            addPost();
          }
        }

      } else {
        addPost();
      }

    }

    function addPost() {
      if(vm.editMode == true){

        if(vm.post.status == '-1' && $rootScope.currentUser.role == 'user' || vm.post.status == '1' && $rootScope.currentUser.role == 'user' && moderation || vm.post.status == -1 && $rootScope.currentUser.role == 'user' || vm.post.status == 1 && $rootScope.currentUser.role == 'user' && moderation){
          vm.post.status = '0';
        }
        vm.post.attachments = vm.attachments;


        var author = vm.post.author;
        var actualAuthor = vm.post.actualAuthor;
        vm.post.author = vm.post.author._id;
        vm.post.actualAuthor = vm.post.actualAuthor._id;

        CommunityService.updatePost(vm.post)
          .then(function(success){

            vm.post.author = author;
            vm.post.actualAuthor = actualAuthor;

            vm.refreshFeed();
            vm.postModal.hide();
            $timeout(function(){
              $ionicLoading.hide();
            },1000);
          }, function(error){
            console.log(error);
            alert(error);
            $ionicLoading.hide();
          })
      } else {

        if($rootScope.currentUser.role == 'admin'){

        } else if(moderation == true && $rootScope.currentUser.role == 'user'){
          vm.post.status = '0';
        } else {
          vm.post.status = '1';
        }

        vm.post.attachments = vm.attachments;
        CommunityService.addPost(vm.post)
          .then(function(success){
            vm.post._id = success.data.insertedIds[0];
            subscribe(vm.post, true);
            scrollTop();
            vm.refreshFeed();
            vm.postModal.hide();
            $timeout(function(){
              $ionicLoading.hide();
            },1000);
          }, function(error){
            console.log(error);
            alert(error);
            $ionicLoading.hide();
          })
      }
    }

  }

  function deletePost () {
    vm.morePopover.hide();

    // A confirm dialog
    var showConfirm = $scope.showConfirm = function() {
      $ionicPopup.confirm({
        title: 'Delete Post',
        template: 'Are you sure you want to delete this post?'
      }).then(function(res) {
        if(res) deletePostConfirmed();
      });
    };
    showConfirm();
    function deletePostConfirmed () {
      $ionicLoading.show();
      CommunityService.deletePost(vm.post._id)
        .then(function(success){
          $ionicLoading.hide();

          if(currentState == 'app.community'){
            vm.refreshFeed();
          } else {
            $ionicHistory.goBack();
          }

        }, function(error){
          console.log(error);
        })
    }

  }

  function likePost (post) {
    if(post.liked != true){
      subscribe(post, true);
      CommunityService.addLike(post._id)
        .then(function(success){
          if(currentState == 'app.community'){
            for (var i = vm.posts.length - 1; i >= 0; i--) {
              if(vm.posts[i]._id == post._id) {
                vm.posts[i].liked = true;
                vm.posts[i].likes.push($rootScope.currentUser._id);
              }
            }
          } else {
            vm.post.liked = true;
            // vm.post.likes.push({date:new Date(),user:$rootScope.currentUser, _id:success.data.ops[0]._id});
            getLikes();
          }

        }, function(error){
          console.log(error);
        })

    } else {
      CommunityService.deleteLike(post._id)
        .then(function(success){
          if(currentState == 'app.community'){
            for (var i = vm.posts.length - 1; i >= 0; i--) {
              if(vm.posts[i]._id == post._id) {
                  vm.posts[i].liked = false;
                  vm.posts[i].likes.splice( vm.posts[i].likes.indexOf($rootScope.currentUser._id),1 );
              }
            }
          } else {
            vm.post.liked = false;
            // for (var i = vm.post.likes.length - 1; i >= 0; i--) {
            //   if(vm.post.likes[i].user._id == $rootScope.currentUser._id) {
            //     vm.post.likes.splice(i,1);
            //   }
            // }
            getLikes();
          }
        }, function(error){
          console.log(error);
        })

    }

  }

  function scrollTop () {
    $ionicScrollDelegate.scrollTop(true);
  }

  function openCommentModal (comment) {
    if(comment){
      vm.closeCommentMorePopover();
      vm.comment = comment;
    } else {
      vm.comment = {body:""}
    }
    $ionicModal.fromTemplateUrl('new-comment-modal.html', {
      scope: $scope,
      animation: 'slide-in-up',
      backdropClickToClose: false
    }).then(function(modal) {
      vm.commentModal = modal;
      vm.commentModal.show();
    });
  };
  function closeCommentModal () {
    vm.commentModal.hide();
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    if(vm.commentModal) vm.commentModal.remove();
  });

  function postComment (comment) {

    if(!comment.post){
      subscribe(vm.post, true);
      CommunityService.addComment(vm.post._id, comment.body)
        .then(function(success){
          // vm.post.comments.push(success.data.ops[0])
          vm.commentModal.hide();

          $timeout(function(){
            $ionicScrollDelegate.scrollBottom(true);
          },100);
          vm.comment = {body:""}

          getComments();
        }, function(error){
          console.log(error);
        })
    } else {
      CommunityService.updateComment(comment)
        .then(function(success){
          vm.commentModal.hide();
          vm.comment = {body:""}
        }, function(error){
          console.log(error);
        })
    }

  }

  function deleteComment (comment) {
    vm.closeCommentMorePopover();

    // A confirm dialog
    var showConfirm = $scope.showConfirm = function() {
      $ionicPopup.confirm({
        title: 'Delete Comment',
        template: 'Are you sure you want to delete this comment?'
      }).then(function(res) {
        if(res) deleteCommentConfirmed();
      });
    };
    showConfirm();
    function deleteCommentConfirmed () {
      CommunityService.deleteComment(comment._id)
        .then(function(success){
          for (var i = vm.post.comments.length - 1; i >= 0; i--) {
            if(vm.post.comments[i]._id == comment._id){
              vm.post.comments.splice(i,1);
            }
          }
        }, function(error){
          console.log(error);
        })
    }

  }

  function openCommentMorePopover ($event, comment) {
    // .fromTemplateUrl() method
    $ionicPopover.fromTemplateUrl('more-comment-popover.html', {
      scope: $scope
    }).then(function(popover) {
      vm.comment = comment;
      vm.moreCommentPopover = popover;
      vm.moreCommentPopover.show($event);
    });
  };

  function closeCommentMorePopover () {
    vm.moreCommentPopover.hide();
  };
  //Cleanup the popover when we're done with it!
  $scope.$on('$destroy', function() {
    if(vm.moreCommentPopover) vm.moreCommentPopover.remove();
  });

  function getLikes() {
    vm.loadingLikes = true;
    CommunityService.getLikes(vm.post._id)
      .then(function(success){
        vm.post.likes = success.data;
        vm.loadingLikes = false;
      }, function(error){
        console.log(error);
      })
  }
  function getComments(scroll) {
    vm.loadingComments = true;
    CommunityService.getComments(vm.post._id)
      .then(function(success){
        vm.post.comments = success.data;
        vm.loadingComments = false;

        if(scroll == true) {
          $ionicScrollDelegate.scrollBottom(true);
        }

      }, function(error){
        console.log(error);
      })
  }

  function subscribe (post, check) {

    if(post.subscribed != true){
      CommunityService.addSubscription(post._id)
        .then(function(success){
          if(currentState == 'app.community'){
            for (var i = vm.posts.length - 1; i >= 0; i--) {
              if(vm.posts[i]._id == post._id) {
                vm.posts[i].subscribed = true;
                vm.posts[i].subscribe.push($rootScope.currentUser._id);
              }
            }
          } else {
            vm.post.subscribed = true;
          }

        }, function(error){
          console.log(error);
        })

    } else if (!check) {
      CommunityService.deleteSubscription(post._id)
        .then(function(success){
          if(currentState == 'app.community'){
            for (var i = vm.posts.length - 1; i >= 0; i--) {
              if(vm.posts[i]._id == post._id) {
                  vm.posts[i].subscribed = false;
                  vm.posts[i].subscribe.splice( vm.posts[i].subscribe.indexOf($rootScope.currentUser._id),1 );
              }
            }
          } else {
            vm.post.subscribed = false;
          }
        }, function(error){
          console.log(error);
        })

    }

  }


  // Gallery Modal
  function openGalleryModal (post) {

    $ionicModal.fromTemplateUrl('modules/community/_imageGallery.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      vm.galleryModal = modal;
      if(post) {
        vm.post = post;
      }
      if(vm.post.attachments.length == 1 && vm.post.attachments[0].type == 'video'){
        openVideo(vm.post.attachments[0]);
      } else {
        vm.galleryModal.show();
        $rootScope.isFullScreenMode = true;
        if (window.StatusBar) StatusBar.hide();
        if(window.cordova) screen.unlockOrientation();
      }
    });


  };

  function closeGalleryModal () {
    $ionicSlideBoxDelegate.slide(0);
    if (window.StatusBar) StatusBar.show();
    $rootScope.isFullScreenMode = false;
    vm.galleryModal.remove();
    if(window.cordova) screen.lockOrientation('portrait');
  };
  // Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    if(vm.galleryModal) vm.galleryModal.remove();
  });

  function openVideo (image) {
    if(image.type == 'video'){
      var url = image.filename;
      window.plugins.streamingMedia.playVideo(url);
    }
  }

  function slideChanged (index) {
    vm.currentGallerySlide = index;
  }

  function slideVisible(index){
    if(  index < $ionicSlideBoxDelegate.currentIndex() -1
       || index > $ionicSlideBoxDelegate.currentIndex() + 1){
      return false;
    }
    return true;
  }

  function doOnOrientationChange()
  {
    if($rootScope.isFullScreenMode)
      $timeout(function() {
        $ionicSlideBoxDelegate.update();
      }, 500);
  }

  $scope.generateAvatarColor = function(fullName) {

		if(fullName){
			var numColors = 11;
	    var hash = 0;
	    if(fullName.length == 0) return hash;
	    for(i = 0; i < fullName.length; i++){
	        char = fullName.charCodeAt(i);
	        hash = ((hash<<5)-hash)+char;
	        hash = hash & hash; // Convert to 32bit integer
	    }
	    hash = Math.abs(hash) % numColors;

	    return hash;
		}

  }

  $ionicPlatform.ready(function() {
    window.addEventListener('orientationchange', doOnOrientationChange);
  }, false);

})
