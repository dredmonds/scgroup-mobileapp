angular.module('starter.controllers')

.controller('SettingsController', function(SigninService, $cordovaNativeStorage, $state, $rootScope, $ionicSlideBoxDelegate, $ionicModal, $timeout, $http) {

	// www

	var vm = this;

	vm.signout = signout;

	function signout () {


		SigninService.unregisterPush()
				.then(function(success){

		        SigninService.signOut()
		            .then(function(success){
		                $rootScope.currentUser = null;
										delete localStorage.communities;
										delete localStorage.currentUser;
										delete localStorage.token;
		                $cordovaNativeStorage.remove("localStorage");
		                $state.go("app.signin");
		            }, function(error){
		            })

				}, function(error){
				})

	}


    // var request = $http({
    //     method: "post",
    //     url: localStorage["serverURL"]+"push/register",
    //     params: {},
    //     data: {token:"registrationId"},
    //         headers:{
    //             "content-type":"application/json"
    //         },
    // });

    // request.then(function(result) {
    //     console.log(result)
    //     alert("push saved")
    //   }, function(error) {
    //     console.log(error)
    //     alert("push not saved")
    //   })


})
