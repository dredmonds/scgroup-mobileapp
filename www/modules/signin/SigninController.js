angular.module('starter.controllers')

.controller('SigninController', function(SigninService, $scope, $ionicModal, $timeout, $ionicPopup, $state, $ionicLoading, $rootScope) {

	if(!$rootScope.isSplashScreenHide) {
		$timeout(function() {
	    	$rootScope.isSplashScreenHide = true;
	        navigator.splashscreen.hide();
	    }, 1500);
	}

	$scope.authenticateUser = function (user) {

	    var missingFields = [];
	    if (!user || !user.username) {
	      missingFields.push("<br>Email address missing");
	    }
	    if (!user || !user.password) {
	      missingFields.push("<br>Password missing");
	    }
	    if(missingFields.length > 0){
	        $ionicPopup.alert({
	          title: missingFields.toString().replace(/,/g , " "),
	        });
	    } else {
	    	$ionicLoading.show();
	    	user.username = user.username.toLowerCase();
	        SigninService.authenticate(user)
	            .then(function(success){
	            	$ionicLoading.hide();
	            	if(success.data.user.pin != null){
				        $ionicPopup.alert({
				          title: 'Verify account',
				          template: '<center>You have been sent an email containing an activation link. Please use this link to verify your account before logging in.</center>'
				        });
	            	} else if(success.data.user.tempPass) {
	            		$scope.changePassword(user, success.data.token, success.data.user);
	            	} else {
		            	localStorage["token"] = JSON.stringify(success.data.token);
		            	localStorage["currentUser"] = JSON.stringify(success.data.user);
		            	$state.go("app.homescreen");
	            	}
	            }, function( errorMessage ) {
	            	$ionicLoading.hide();
								console.warn( errorMessage );
								if(errorMessage.data == null || errorMessage.data == undefined) {
										errorMessage.data = 'Please check your network connection.';
								}
				        $ionicPopup.alert({
				          title: "Login failed",
				          template: '<center>'+errorMessage.data+'</center>'
				        });
	            });
	    }

	}


	$scope.changePassword = function (user, token, currentUser) {

		$scope.data = {
			username:user.username,
			oldPassword:user.password,
			password:'',
		};

		var myPopup = $ionicPopup.show({
			template: '<input class="changepasswordinput" type="password" ng-model="data.password" placeholder="Password" style="margin: 0;padding-left:10px; background: #f4f4f4; border-radius: 5px;"><br><input class="changepasswordinput" type="password" ng-model="data.confirmPassword" placeholder="Retype password" style="margin: 0;padding-left:10px; background: #f4f4f4; border-radius: 5px;">',
			title: 'Change password',
			subTitle: 'Please type a new password below to proceed.',
			scope: $scope,
			buttons: [
			  { text: 'Cancel' },
			  {
			    text: '<b>Continue</b>',
			    type: 'button-positive',
			    onTap: function(e) {
					if (!$scope.data.password || $scope.data.password != $scope.data.confirmPassword) {
						//don't allow the user to close unless he enters wifi password
						$ionicPopup.alert({
							title: '!',
							template: '<center>Passwords must match</center>'
						});
						e.preventDefault();
					} else {

						$ionicLoading.show();
						// return
				        SigninService.resetPassword($scope.data)
				            .then(function(success){
				            	myPopup.close();
				            	localStorage["token"] = JSON.stringify(token);
				            	localStorage["currentUser"] = JSON.stringify(currentUser);
				            	$state.go("app.homescreen");
							  	$ionicLoading.hide();
				            }, function( errorMessage ) {
				            	$ionicLoading.hide();
				                console.warn( errorMessage );
						        $ionicPopup.alert({
						          title: "Password error",
						          template: '<center>'+errorMessage.data+'</center>'
						        });
				            });
				            e.preventDefault();

			      	}
			    }
			  }
			]
		});

	}

	$scope.requestPassword = function() {
	  $scope.userObject = {}

	  // An elaborate, custom popup
	  var showRequestPass = $ionicPopup.show({
		template: '<div>'+
					'<span class="center-text">Please enter an email address for your account.</span>'+
					'<br /><br /><input type="email" placeholder="Account email address" ng-model="userObject.username" class="fp-input" lowercased style="margin: 0;padding-left:10px; background: #f4f4f4; border-radius: 5px;">'+
				  '</div>',
	    title: 'Need your password?',
	    subTitle: '',
	    scope: $scope,
	    buttons: [
	      { text: 'Cancel' },
	      {
	        text: '<b>Ok</b>',
	        type: 'button-positive',
	        onTap: function(e) {

			   	if ( !$scope.userObject.username || !validateEmail($scope.userObject.username) ) {
					$ionicLoading.show({ template: 'Please enter a valid email address' });
					$timeout(function(){
						$ionicLoading.hide();
					},1000);
		            e.preventDefault();
	          	} else {
				  	showRequestPass.close();
				  	$ionicLoading.show({ template: 'Please wait...' });
				  	var user = { username: $scope.userObject.username };
			        SigninService.resetPassword(user)
			            .then(function(success){

						  	$ionicLoading.hide();
							$ionicPopup.alert({
								title: '',
								template: '<center>Instructions to retrieve your password have been sent to your email address.</center>'
							});
			            }, function( errorMessage ) {
			            	$ionicLoading.hide();
			                console.warn( errorMessage );
					        $ionicPopup.alert({
					          title: "Request failed",
					          template: '<center>There is no account associated with this email address</center>'
					        });
			            });

	          }
	        }
	      }
	    ]
	  });

	 };




	 $scope.signinRoute = function () {
	 	if($scope.config.signup.methods.companydomain && $scope.config.signup.methods.companydomain.enabled == true) {
	 		console.log($scope.config.signup.methods)
	 		$state.go("app.signup");
	 	} else if ($scope.config.signup.methods.nondesk && $scope.config.signup.methods.nondesk.enabled == true) {
	 		$state.go("app.signup-verification");
	 	}
	 }

})


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    // if(appConfig.userManagement.emailDomains && re.test(email) == true){
    if(re.test(email) == true){

    	var emailDomain = email.split("@").pop();

    	// for (var i = 0; i < appConfig.userManagement.emailDomains.length; i++) {
    		// if(appConfig.userManagement.emailDomains[i] == emailDomain || "staffconnectapp.com" == emailDomain){
    			return true;
    		// }
    	// }
    	// return false;

    } else {
    	return re.test(email);
    }


}
