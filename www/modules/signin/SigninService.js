angular.module('starter.services')

.service('SigninService', function( $http, $q, $rootScope, $state ) {

	// Return public API.
	return({
	    authenticate: authenticateUser,
	    create: createUser,
	    // get: getUser,
	    update: updateUser,
	    getCurrentUser: getCurrentUser,
	    // delete: deleteUser,
	    resetPassword:resetPassword,
	    signOut:signOutUser,
			unregisterPush:unregisterPush,
	});

	function createUser(userObject) {
	    var request = $http({
	        method: "post",
	        url: localStorage["serverURL"]+"auth/register",
	        data: userObject
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function authenticateUser(userObject) {
	    var request = $http({
	        method: "post",
	        url: localStorage["serverURL"]+"auth/login",
	        headers:{
	        	"content-type":"application/json"
	        },
	        data: userObject
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function signOutUser() {
	    var request = $http({
	        method: "post",
	        url: localStorage["serverURL"]+"auth/logout",
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function unregisterPush() {
	    var request = $http({
	        method: "post",
	        url: localStorage["serverURL"]+"push/unregister",
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function resetPassword(userObject) {
	    var request = $http({
	        method: "post",
	        url: localStorage["serverURL"]+"auth/forgotpass",
	        data: userObject
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updateUser(id) {
	    var request = $http({
	        method: "get",
	        url: localStorage["serverURL"]+"auth/login",
	        params: {
	            action: "get"
	        },
	        data: {
	            id: id
	        }
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	// function getUser(id) {
	//     var request = $http({
	//         method: "get",
	//         url: "api/index.cfm",
	//         params: {
	//             action: "get"
	//         },
	//         data: {
	//             id: id
	//         }
	//     });
	//     return( request.then( handleSuccess, handleError ) );
	// }

	function getCurrentUser(id) {
		var currentUser = JSON.parse(localStorage["currentUser"]);
	    return currentUser;
	}

	// function deleteUser( id ) {
	//     var request = $http({
	//         method: "delete",
	//         url: "api/index.cfm",
	//         params: {
	//             action: "delete"
	//         },
	//         data: {
	//             id: id
	//         }
	//     });
	//     return( request.then( handleSuccess, handleError ) );
	// }

	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});
