angular.module('starter.controllers')

.controller('QuestionsController', function(QuestionsService, $scope, $ionicModal,  $state, $rootScope, $ionicSlideBoxDelegate, $ionicLoading, $timeout, $http, $stateParams, $ionicPopup) {

    // Getting module options from config file
    for (var i = 0; i < $scope.config.features.length; i++) {
        if($scope.config.features[i].index == $state.params.featureIndex){
            $scope.feature = $scope.config.features[i];
        }
    }

    var vm = this;

    vm.question = {originalQuestion:""}
    vm.openQuestionModal = openQuestionModal;
    vm.closeQuestionModal = closeQuestionModal;
    vm.postQuestion = postQuestion;

    
      $scope.$on('$ionicView.enter', function(e) {
          getQuestions();
      });

    function getQuestions () {
        QuestionsService.getQuestions()
            .then(function(success){
                vm.questions = success.data;
            }, function(error){
              console.log(error);
            })
    }

      function openQuestionModal (question) {
        vm.question = {originalQuestion:""}
        $ionicModal.fromTemplateUrl('new-question-modal.html', {
          scope: $scope,
          animation: 'slide-in-up',
          backdropClickToClose: false
        }).then(function(modal) {
          vm.questionModal = modal;
          vm.questionModal.show();
        });
      };

      function closeQuestionModal () {
        vm.questionModal.hide();
      };
      // Cleanup the modal when we're done with it!
      $scope.$on('$destroy', function() {
        if(vm.questionModal) vm.questionModal.remove();
      });

      function postQuestion (question) {
          console.log(question)
          QuestionsService.postQuestion(question)
            .then(function(success){
              vm.questionModal.hide();
              vm.question = {originalQuestion:""}
              $ionicPopup.alert({
                title: 'Question Submitted',
                template: '<center>Thank you for submitting a question, we will respond shortly. Come back later to see the answer.</center>'
              });
            }, function(error){
              console.log(error);
            })

      }

})