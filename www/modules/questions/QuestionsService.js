angular.module('starter.services')
	
.service('QuestionsService', function( $http, $q, $rootScope) {

	// Return public API.
	return({
	    getQuestions: getQuestions,
	    postQuestion: postQuestion,
	});


	function getQuestions() {

	    var request = $http({
	        method: "GET",
	        url: localStorage["serverURL"]+"questions",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function postQuestion(data) {

	    var request = $http({
	        method: "post",
	        url: localStorage["serverURL"]+"questions",
	        params: {},
	        data: data
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});