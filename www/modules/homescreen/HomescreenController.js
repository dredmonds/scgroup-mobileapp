angular.module('starter.controllers')

.controller('HomescreenController', function(HomescreenService, $scope,$http, $rootScope, $ionicPlatform, $ionicHistory, $ionicSlideBoxDelegate, $ionicModal, $timeout, $ionicLoading, $state) {

    function initAnalytics (){
        if(window.cordova && $rootScope.currentUser){
            // $ionicPlatform.ready(function(){
                // window.ga.debugMode();
                window.ga.startTrackerWithId($rootScope.config.global.googleAnalytics.trackingId);
                window.ga.setUserId($rootScope.currentUser._id);
                // $cordovaGoogleAnalytics.setAllowIDFACollection(true);
            // });
        }
    }

    $ionicPlatform.ready(initAnalytics);

    $scope.loadedImageCount = 0;
    $scope.ImageLoaded = function(value) {
        if($rootScope.isSplashScreenHide) return;
        if(value)
            $scope.loadedImageCount++;
        if($scope.loadedImageCount == $rootScope.config.homescreen.panels.length) {
            $rootScope.isSplashScreenHide = true;
            $timeout(function() {
                navigator.splashscreen.hide();
            }, 2500);
        }
    }

    function hideSplashScreen() {
        if($rootScope.isSplashScreenHide) return;
        $rootScope.isSplashScreenHide = true;
        $timeout(function() {
            if($scope.loadedImageCount < $rootScope.config.homescreen.panels.length) {
                navigator.splashscreen.hide();
            }
        }, 5000);
    }

    $ionicPlatform.ready(hideSplashScreen);

    $scope.$on('$ionicView.enter', function(e) {

        $ionicHistory.clearHistory();
        $scope.tickertape = "";
        getTicker();
    });

    $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        if(fromState.name == 'app.signin') {
            registerForPush();
            initAnalytics();
        }
    });

    var currentPlatform = ionic.Platform.platform();
    function registerForPush () {

        if(localStorage.registrationId && localStorage.registrationId.length > 0){
            $http.post(localStorage["serverURL"] + 'push/register', {token:localStorage.registrationId, platform:currentPlatform})
              .then(function(result) {
                console.log(result)
              }, function(error) {
                console.log(error)
                $timeout(registerForPush,10000);
              })
        } else {
            $timeout(registerForPush,1000);
        }
    }
    $ionicPlatform.ready(registerForPush);

	// Panel Height
  var deviceHeight = window.innerHeight;
  var panelHeight;
  if($rootScope.config.homescreen.panels.length == 1) {
    panelHeight= deviceHeight*0.7;
  } else {
    panelHeight= deviceHeight*0.35;
  }
  $scope.panelHeight = panelHeight;

	// Saving slide position
	$scope.saveSlidePosition = function(){
		$rootScope.slideIndex = $ionicSlideBoxDelegate.currentIndex();
	}



    // Marquee mover
/*    function marquee(speed) {
        var count = document.documentElement.clientWidth;
        setInterval(function() {
            var test = document.getElementById("marquee");
            if (test == null || test == undefined) return false;
            var width = (test.clientWidth + 1);
            if (count <= -width) {
            count = document.documentElement.clientWidth;
            }
            count-=1;
            test.style.left = count + 'px';
        }, speed)
    }*/

    function marquee(text, speed) {
        var documentClientWidth = document.documentElement.clientWidth;
        var marquee_wrapper = document.getElementById("marquee");
        var marquee_text = document.getElementById("marquee_text");
        marquee_wrapper.innerHTML = "";
        //Todo | Fredrick : spped / 80 is the time for one letter. I think we should define this.
        // var duration = text.length * speed / 100;
        var duration = speed;
        if(document.getElementById("marquee_id") == null) {
            var style = document.createElement('style');
            style.id = "marquee_id";
            style.type = 'text/css';
            style.innerHTML = '.marquee_content { animation: marquee ' + duration + 's linear infinite; -webkit-animation: marquee ' + duration + 's linear infinite; -moz-animation: marquee ' + duration + 's linear infinite; }';
            var head = document.getElementsByTagName('head')[0];
            head.appendChild(style);
            marquee_text.className = 'marquee_content';
        }
        marquee_wrapper.appendChild(marquee_text);
    }


    function getTicker () {
        HomescreenService.getTicker()
            .then(function(success){
                $scope.tickertape = success.data.value;
                var speedDelay = $scope.config.homescreen.tickertape.speed;
                if(speedDelay == null || speedDelay == undefined){
                  speedDelay = 20; //Set to default speed delay/ms
                }
                marquee($scope.tickertape, speedDelay);
            }, function(error){
              console.log(error);
            })
    }


    $scope.navigateToModule=function(button){
      //$state.go("app."+button.module+"({featureIndex: "+button.index+", id: "+button.moduleId+"})");
      if(button.module == 'externalLink'){
        $scope.iAB(button.externalLinkURL);
      }else{
        $state.go("app."+button.module, {featureIndex: button.index, id:button.moduleId});
      }

    }

})
