angular.module('starter.controllers')

.controller('QuizController', function(LibraryService, $scope, $state, $rootScope, $ionicSlideBoxDelegate, $ionicModal, $timeout, $http, $stateParams, $ionicTabsDelegate, $ionicHistory) {

    // Getting module options from config file
    for (var i = 0; i < $scope.config.features.length; i++) {
        if($scope.config.features[i].index == $state.params.featureIndex){
            $scope.feature = $scope.config.features[i];
        }
    }

    var vm = this;
    vm.feature = $scope.config.features[$stateParams.featureIndex];
    vm.currentUser = $rootScope.currentUser;
    vm.featureIndex = $stateParams.featureIndex;
    vm.activeQuizList = [];

 		$http({
        	method:"GET",
	        url: localStorage["serverURL"]+'quiz/all',
	        params: {},
	        data: {}
        }).then(function(quiz) {
        	var quizList = quiz.data;
            
            for(var i in quizList){
    				if(quizList[i].type == 0 && quizList[i].published == 1){
    					vm.activeQuizList.push(quizList[i]);
    				}
    			}
      }, function(error) {
        console.log(error);
      })

})
  .controller('QuizQuestionsController', function ($scope, $rootScope, $http, $stateParams, $state, $ionicPopup, $timeout) {
    var vm = this;
    vm.feature = $scope.config.features[$stateParams.featureIndex];
    vm.currentUser = $rootScope.currentUser;
    vm.featureIndex = $stateParams.featureIndex;

    var timeStarted = false;
    var points = 0;
    $scope.disableButton = false;
    $http({
        	method:"GET",
	        url: localStorage["serverURL"]+'quiz/'+$stateParams.id,
	        params: {},
	        data: {}
        }).then(function(result) {
        
        $scope.quizData = result.data;
       
        $scope.quesArray = $scope.quizData['questions'];
        $scope.ansArray = $scope.quizData['answers'];

        $scope.numberOfQues = $scope.quesArray.length;
        $scope.totalQues = $scope.quesArray.length;
        $scope.question_index = 0;
        
        $scope.dataArr = [];
        $scope.next = function(q_id, a_id, numberOfQues){
          $scope.disableButton = true; 
          $scope.numberOfQues = (numberOfQues + 1);

            if ($scope.quizData.info[0].type==0) {
              $scope.dataArr.push({ question: q_id, answer: a_id });

              for (var i in $scope.ansArray) {
                
                if(($scope.ansArray[i].quiz_question == q_id) && ($scope.ansArray[i].correct == 1)){

                   
                   if($scope.ansArray[i].id == a_id){
                    document.getElementById('aid_'+ $scope.ansArray[i].id).className += " correctAns";
                     points++; 
                     console.log("correct, points:", points);
                    } 
                    $timeout(function(){ 
                        if(($scope.question_index >= $scope.quesArray.length - 1) && ($scope.numberOfQues == $scope.quesArray.length)){
                          $scope.question_index = 0;
                        }else{
                          $scope.question_index++;
                          $scope.disableButton = false;
                        }
                      }, 800); 
                    } else{
                        if($scope.ansArray[i].id == a_id ){
                          document.getElementById('aid_'+ $scope.ansArray[i].id).className += " wrongAns";
                        }
                    }
                  }
               }
         
        if($scope.numberOfQues == $scope.quesArray.length){
            $scope.stopClock();
            $timeout(function(){ 
              $scope.showResults();
            }, 800); 
          }
        }; //end of next

      }, function(error) {
        console.log(error);
      })

      // Show results
      $scope.showResults = function() {
        $scope.messageToUser = 'test';

        var confirmPopup = $ionicPopup.confirm({
          title: '<div style="color:'+$rootScope.config.global.theme.popup.title+' !important;">Your score</div>',
          cssClass: 'quiz_popup',
          template: 
          '<div class="your_score">' +
            '<div class="tola_score">'+
              '<div class="tola_quiz_count">' + points + '/' + $scope.numberOfQues + '</div>'+
                '<span class="in">in</span>'+
                '<div class="popup_time">' + $scope.timeStopped + '</div>'+
            '</div>'+
            'Do you want to share your score to the leaderboard?'+
          '</div>',
          buttons:[
            { 
              text: 'No thanks',
              type: 'button-stable',
              onTap: function(e) {
                $state.go('app.quiz', {featureIndex: vm.featureIndex});
              }
            },
            {
              text: 'Yes',
              type: 'button-positive',
              onTap: function(e) {
                
                $http({
                method:"POST",
                headers:{
                  "content-type":"application/json"
                },
                url: localStorage["serverURL"] + 'quiz/response',
                params: {},
                data: {user: vm.currentUser.username,firstname: vm.currentUser.first_name,lastname: vm.currentUser.last_name,quiz: $stateParams.id,answer: JSON.stringify($scope.dataArr),points: points,time:$scope.seconds}
              }).then(function(result) {                 
                      console.log(result);
                      $state.go('app.leaderboard', {featureIndex: vm.featureIndex, id: $stateParams.id});
                    }, function(error) {
                      console.log(error);
                });
              }
            }
          ]
        });
        
      };

      var startClock = function() {
        if (!timeStarted) {
            $scope.$broadcast('timer-start');
            $scope.timerRunning = true;
            timeStarted = true
        } else if ((timeStarted) && (!$scope.timerRunning)) {
            $scope.$broadcast('timer-resume');
            $scope.timerRunning = true;
        }

      };
      $timeout(startClock, 500);

        $scope.stopClock = function() {
            if ((timeStarted) && ($scope.timerRunning)) {
              
                $scope.$broadcast('timer-stop');
                $scope.timerRunning = false;
            }

        };

        $scope.resetClock = function() {
            if (!$scope.timerRunning){
                $scope.$broadcast('timer-reset');
            }
      }

      function pad(str, max) {
        str = str.toString();
        return str.length < max ? pad("0" + str, max) : str;
      }

      $scope.$on('timer-stopped', function(event, data) {
          timeStarted = true;
          $scope.timeStopped = pad(data.minutes, 2) + ":" + pad(data.seconds, 2);
          // convert time to seconds
          var hms = pad(data.hours, 2) + ":" + pad(data.minutes, 2) + ":" + pad(data.seconds, 2);   
          var a = hms.split(':'); 
          $scope.seconds = (+a[0]) * 60 * 60 + (+a[1]) * 60 + (+a[2]);          
      });

  }).filter('isEmpty', [function() {
  return function(object) {
    return angular.equals({}, object);
  }
}]).controller('leaderboard', function ($scope, $rootScope, $http, $stateParams, $state, $ionicPopup, $timeout) {
    var vm = this;
    vm.feature = $scope.config.features[$stateParams.featureIndex];
    vm.currentUser = $rootScope.currentUser;
    vm.featureIndex = $stateParams.featureIndex;

    $http({
          method:"GET",
          url: localStorage["serverURL"]+'quiz/analytics/'+$stateParams.id,
          params: {},
          data: {}
        }).then(function(result) {

      $scope.leaderboardData = result['data'];
      var arr = {};

      for ( var i=0, len=$scope.leaderboardData.length; i < len; i++ )
          arr[$scope.leaderboardData[i]['user']] = $scope.leaderboardData[i];

      $scope.leaderboardData = new Array();
      for ( var key in arr )
          $scope.leaderboardData.push(arr[key]);      
    }, function(error) {
       console.log(error);
    })


    $http({
      method:"GET",
      url: localStorage["serverURL"]+'quiz/'+$stateParams.id,
      params: {},
      data: {}
    }).then(function(result) {
        vm.quesLength = result['data'].questions.length;
        vm.quizName = result['data']['info']['0']['name'];
    }, function(error) {
       console.log(error);
    })


})
.filter('secondsToHHmmss', function($filter) {
    return function(seconds) {
        return $filter('date')(new Date(0, 0, 0).setSeconds(seconds), 'mm:ss');
    };
})

