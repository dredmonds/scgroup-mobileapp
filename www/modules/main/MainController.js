angular.module('starter.controllers', [])

.controller('MainController', function(StartService, $ionicLoading, $scope, SigninService, $ionicPlatform, $rootScope, $ionicModal, $timeout, $state, $cordovaInAppBrowser, $sce, $ionicPopup, $http, $ionicSlideBoxDelegate) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  var vm = this;
  $scope.foo = "bar";

  $scope.$on('$ionicView.beforeEnter', function(e) {
    $scope.currentView = $state.current.name;

    if(localStorage["currentUser"]){
      $rootScope.currentUser = JSON.parse(localStorage["currentUser"]);
    }

    if($state.current.name != 'app.start'){
      $scope.currentView == 'app.homescreen'
    }

    updateConfig();

    if(window.cordova) screen.lockOrientation('portrait');

    $rootScope.imgixAssetURL = localStorage.serverURL && localStorage.serverURL.includes("staging") ? 'https://staffconnectconfig.imgix.net/'+localStorage.databaseName+'-staging':'https://staffconnectconfig.imgix.net/'+localStorage.databaseName;

  });

  $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {

    if($rootScope.currentUser && window.cordova){
      if(toState.name == 'app.community' || toState.name == 'app.rssfeed' || toState.name == 'app.library') {
        var name = $rootScope.config.features[toParams.featureIndex].name;
      } else {
        var name = toState.name.substring(4);
      }
      console.log(name)
      window.ga.trackView(name);
    }

  });

  if(localStorage["currentUser"]){
    $rootScope.currentUser = JSON.parse(localStorage["currentUser"]);
  }

  if(localStorage["config"]){
    $rootScope.config = JSON.parse(localStorage["config"]);
  }

  // inAppBrowser
  /*alert('in controller');*/
  $scope.iAB = iAB;
  function iAB (url, unlockOrientation, pdf) {
    /*alert('in function');*/
    var options = {
        /*location: 'yes',*/
        location: 'no',
        /*clearcache: 'no',*/
        clearcache: 'no',
        /*clearsessioncache: 'no',*/
        toolbar: 'yes',
        enableViewportScale:'yes',
        closebuttoncaption: 'Back',
        toolbarposition:'top'
        /*toolbar: 'balanced'*/

    };
    if(unlockOrientation) { screen.unlockOrientation(); }

    if(ionic.Platform.isAndroid() && pdf){
        $cordovaInAppBrowser.open('https://docs.google.com/viewer?url=' + url + '&embedded=true', '_blank', options);
    } else if( ionic.Platform.isAndroid() ) {
        $cordovaInAppBrowser.open(url, '_blank', options);
    } else {
        var ref = cordova.InAppBrowser.open(url, '_blank', 'location=no,clearcache=no,toolbar=yes,enableViewportScale=yes,closebuttoncaption=Back,toolbarposition=top');
        ref.addEventListener('exit', function(event) { screen.lockOrientation('portrait'); });
    }

  }

  // toTrusted
  $scope.to_trusted = function(html_code) {
      return $sce.trustAsHtml(html_code);
  }
  function updateConfig() {
    // Handle the resume event
    var orgId = localStorage["orgId"];
    StartService.getServerURL(orgId)
      .then(function(success){
        localStorage["serverURL"] = success.data;
        StartService.getConfigFile(orgId)
          .then(function(success){
            success.data.legacy = JSON.parse( success.data.legacy );
            localStorage["config"] = JSON.stringify( success.data.legacy );
            $rootScope.config = success.data.legacy;

            generateNavMenu();
          });
      });
  }

  // Generating the navigation menu
  //$scope.slides = {};

  function generateNavMenu () {
    var slideCount = 0;
    var featureCount = 0;
    var featuresPerSlide = 3;
    $scope.slides = {}; //Clear navigation menu object
    for (var i = 0; i < $rootScope.config.features.length; i++) {

          if($rootScope.config.features[i].navigation.visible == true){

          if(featureCount == 0){
            $scope.slides[slideCount] = [];
            $scope.slides[slideCount].push($rootScope.config.features[i]);
          } else if (featureCount < featuresPerSlide) {
            $scope.slides[slideCount].push($rootScope.config.features[i]);
          } else {
            featureCount = 0;
            slideCount++;
            $scope.slides[slideCount] = [];
            $scope.slides[slideCount].push($rootScope.config.features[i]);
          }
          featureCount++;

          }
    }
    $ionicSlideBoxDelegate.update();
  }

  $rootScope.$on("signout", function(evt,data){
    if(!$rootScope.unauthorisedAttempt){
      $rootScope.unauthorisedAttempt = true;
      $ionicPopup.alert({
        title: 'User access',
        template: '<center>There is an issue with your account. Please contact the administrator. You have been logged out.</center>'
      });
      SigninService.signOut()
          .then(function(success){
              $rootScope.currentUser = null;
              delete localStorage["currentUser"];
              delete localStorage["token"];
              delete localStorage["communities"];
              $state.go("app.signin");
              $rootScope.unauthorisedAttempt = false;
          }, function(error){
              $rootScope.unauthorisedAttempt = false;
          })
    }
  });

  var currentPlatform = ionic.Platform.platform();
  function checkUpdate () {
    if(window.cordova){
      $http.get(localStorage["serverURL"] + 'version')
        .then(function(result) {
          $scope.version = result.data;
          console.log($scope.version);
          cordova.getAppVersion(function (version) {

              if(version < $scope.version.version[currentPlatform]){
                openUpdateModal();
              }
          });
        }, function(error) {
          console.log(error)
        })
    }
  }

  function onResume () {
    updateConfig();
    checkUpdate();
  }
  $ionicPlatform.ready(checkUpdate);
  $ionicPlatform.on('resume', onResume);


  // Update Modal
  $ionicModal.fromTemplateUrl('modules/main/updateModal.html', {
    scope: $scope,
    animation: 'slide-in-up',
    backdropClickToClose:false
  }).then(function(modal) {
    vm.updateModal = modal;
  });

  function openUpdateModal () {
    vm.updateModal.show();
  };

  function closeUpdateModal () {
    vm.updateModal.hide();
  };

  $scope.closeUpdateModal = closeUpdateModal;

  $scope.updateApp = function () {
    if(currentPlatform == 'android'){
      $cordovaInAppBrowser.open("https://play.google.com/store/apps/details?id=com.staffconnectapp.staffconnect", '_system');
    } else {
      $cordovaInAppBrowser.open("https://itunes.apple.com/gb/app/staffconnect-app/id1175387157?mt=8", '_system');
    }
  }


  // IMGIX cordovaFileTransfer
  $scope.imgixReplace = function (url) {
    if(url.includes("staging")){
      return url.replace("s3-eu-west-1.amazonaws.com/staffconnectstaging", "staffconnectstagings3.imgix.net");
    } else{
      return url.replace("s3-eu-west-1.amazonaws.com/staffconnect", "staffconnects3.imgix.net");
    }
  }
  $rootScope.imgixReplace = $scope.imgixReplace;
  $scope.deviceWidth = window.innerWidth;
  $scope.deviceDPR = window.devicePixelRatio;
  console.log($scope.config)
})
