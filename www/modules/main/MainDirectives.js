angular.module('starter.directives', [])

.directive('compile', ['$compile', '$rootScope', '$state', function ($compile, $rootScope, $state) {

  function parseUrlFilter (text) {
    var linkColor = $rootScope.config.global.theme.linkColor;
    //filter url and email address from content
    var urlPattern = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/gi;
    var emailRegEx = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi;
    var hashtag = /(#[a-z0-9][a-z0-9\-_]*)/gi;
    if($state.current.name == 'app.community' || $state.current.name == 'app.community-detail') {
      return text.replace(urlPattern, "<a class='link-text' style='color:"+linkColor+";' ng-click=\"iAB(\'$&\',true)\">$&</a>").replace(emailRegEx, "<a class='link-text positive' style='color:"+linkColor+";' href=\"mailto:$&\">$&</a>").replace(hashtag, "<a class='link-text' style='color:"+linkColor+";' ui-sref=\"app.community({search:\'$&\'})\">$&</a>");
    } else {
      return text.replace(urlPattern, "<a class='link-text' style='color:"+linkColor+";' ng-click=\"iAB(\'$&\',true)\">$&</a>").replace(emailRegEx, "<a class='link-text positive' style='color:"+linkColor+";' href=\"mailto:$&\">$&</a>");
    }
    return text;
  }

  function removeHtml(text) {

    // Remove html
    text = text.replace(/<\/?[^>]+(>|$)/g, "");

    // Parse linebreaks
    text = text.replace(/\n\r?/g, '<br />');

    // turn links clickable
    return parseUrlFilter(text);
  }

  return function(scope, element, attrs) {
      scope.$watch(
        function(scope) {
          return scope.$eval(attrs.compile);
        },
        function(value) {
          element.html(removeHtml(value));
          $compile(element.contents())(scope);
        }
      )};
}])

.directive('appVersion', function ($ionicPlatform) {
    return function(scope, elm, attrs) {
      $ionicPlatform.ready(function() {
        cordova.getAppVersion(function (version) {
            elm.text(version);
        });
      });
    };
})

.filter('parseUrlFilter', function ($rootScope, $state) {
    var linkColor = $rootScope.config.global.theme.linkColor;
    //filter url and email address from content
    var urlPattern = /(http|ftp|https):\/\/[\w-]+(\.[\w-]+)+([\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;\/~+#-])?/gi;
    var emailRegEx = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi;
    var hashtag = /(#[a-z0-9][a-z0-9\-_]*)/gi;
    return function (text, target, otherProp) {
      if($state.current.name == 'app.community' || $state.current.name == 'app.community-detail') {
        return text.replace(urlPattern, "<a class='link-text' style='color:"+linkColor+";' ng-click=\"iAB(\'$&\',true)\">$&</a>").replace(emailRegEx, "<a class='link-text positive' style='color:"+linkColor+";' href=\"mailto:$&\">$&</a>").replace(hashtag, "<a class='link-text' style='color:"+linkColor+";' ui-sref=\"app.community({search:\'$&\'})\">$&</a>");
      } else {
        return text.replace(urlPattern, "<a class='link-text' style='color:"+linkColor+";' ng-click=\"iAB(\'$&\',true)\">$&</a>").replace(emailRegEx, "<a class='link-text positive' style='color:"+linkColor+";' href=\"mailto:$&\">$&</a>");
      }
    };
})

.directive('prettySubmit', function () {
    return function (scope, element, attr) {
        element.on('submit', (function(event) {
            event.preventDefault();
            element[0].blur();
            if(window.cordova) cordova.plugins.Keyboard.close();
            // alert("blur input")
        }));
    };
})

.filter('linebreaks', ['$filter',
  function($filter) {
    return function(data) {
      if (!data) return data;
      return data.replace(/\n\r?/g, '<br />');
    };
  }
]);
