angular.module('starter.controllers')

.controller('RssfeedController', function(RSSFeedService, $scope, $state, $rootScope, $ionicSlideBoxDelegate, $ionicLoading, $timeout, $http, $stateParams) {

    // Getting module options from config file
    for (var i = 0; i < $scope.config.features.length; i++) {
        if($scope.config.features[i].index == $state.params.featureIndex){
            $scope.feature = $scope.config.features[i];
        }
    }

    var vm = this;
    vm.feedItems = [];
    getFeeds();

    function getFeeds () {
        $ionicLoading.show();
        RSSFeedService.getFeeds()
            .then(function(success){
                vm.feeds = success.data.value;
                getFeedItems(vm.feeds[0])
            }, function(error){
              console.log(error);
            })
    }

    // Using google feed reader to output rss links
    function getFeedItems(item){

        if(item.type == 'custom'){
            var url = item.url;
        } else {
            var url = 'https://query.yahooapis.com/v1/public/yql?q=select%20title%2Clink%2Cdescription%2CpubDate%20from%20rss%20where%20url%3D%22'+encodeURIComponent(item.url)+'%22&format=json';
        }

        $http.get(url)
            .success(function(data) {
                var feedItems = data.query.results.item;

                if(item.type=='custom'){
                  for (var i = 0; i < feedItems.length; i++) {

                    feedItems[i].customData = angular.copy(vm.feeds[0].customData);

                    for (var c = 0; c < feedItems[i].customData.length; c++) {
                      feedItems[i].customData[c].trueValue = getDescendantProp(feedItems[i], feedItems[i].customData[c].value);
                    }

                  }
                }
                vm.feedItems = feedItems;
                $ionicLoading.hide();
            })
            .error(function(data) {
                console.log("ERROR: " + data);
            });
    }

    function getDescendantProp(obj, desc) {
        var arr = desc.split(".");
        while(arr.length && (obj = obj[arr.shift()]));
        return obj;
    }

})
