angular.module('starter.services')
	
.service('EventsService', function( $http, $q, $rootScope) {

	// Return public API.
	return({
	    getDocuments: getDocuments,
	});


	function getDocuments() {

	    var request = $http({
	        method: "GET",
	        url: localStorage["serverURL"]+"library/",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});