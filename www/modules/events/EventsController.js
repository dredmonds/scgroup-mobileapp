angular.module('starter.controllers')
.constant('calendarConfig', {
        formatDay: 'dd',
        formatDayHeader: 'EEE',
        formatDayTitle: 'MMMM dd, yyyy',
        formatWeekTitle: 'MMMM yyyy, Week w',
        formatMonthTitle: 'MMMM yyyy',
        formatWeekViewDayHeader: 'EEE d',
        formatHourColumn: 'ha',
        calendarMode: 'month',
        showWeeks: false,
        showEventDetail: true,
        startingDay: 0,
        eventSource: null,
        queryMode: 'local'
    })
.controller('EventsController', function($scope, $rootScope, $state, $ionicScrollDelegate, $ionicSlideBoxDelegate, $ionicModal, $timeout, $http, $stateParams, $ionicTabsDelegate, $ionicHistory, $filter, $cordovaDialogs, $cordovaInAppBrowser,$interpolate, $log, dateFilter, calendarConfig) {
    var vm = this;
    vm.feature = $scope.config.features[$stateParams.featureIndex];

    // Getting module options from config file
    for (var i = 0; i < $scope.config.features.length; i++) {
        if($scope.config.features[i].index == $state.params.featureIndex){
            $scope.feature = $scope.config.features[i];
        }
    }

    // Imgix parameters
    $scope.imgixParameters = '?auto=format&ch=Save-Data&max-h='+$scope.deviceWidth*1.5*$scope.deviceDPR+'&fit=crop&crop=entropy&min-h='+$scope.deviceWidth/2+'&w='+$scope.deviceWidth;

    $scope.$on('$ionicView.enter', function(e) {
    	$scope.loadEvents();
    });

    'use strict';
    $scope.changeMode = function (mode) {
        $scope.mode = mode;
    };

    $scope.scrollTop = function () {
      $ionicScrollDelegate.scrollTop()
    }

    $scope.today = function () {
        console.log("today",new Date());
        $scope.currentDate = new Date();
    };

    $scope.currentDate = new Date();
    $scope.currentDate = JSON.stringify($scope.currentDate);
    $scope.currentDate = $scope.currentDate.replace(/^"(.*)"$/, '$1');
    $scope.currentMonth = parseInt($filter('date')($scope.currentDate, 'yyyyMM'));

    $scope.isToday = function () {
        var today = new Date(),
            currentCalendarDate = new Date($scope.currentDate);

        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };

    $scope.loadEvents = function () {

        $http({
                method:"GET",
                url: localStorage["serverURL"]+ 'events/',
                params: {},
                data: {}
            }).then(function(resp) {

            var data = JSON.parse(JSON.stringify(resp.data));

            var newArray = {};
            $scope.eventSource = data;

            for (var i = data.length - 1; i >= 0; i--) {
                console.log(data[i].startTime)
                console.log(data[i].endTime)
                console.log($scope.currentDate)

                if($scope.currentDate <= data[i].startTime || $scope.currentDate <= data[i].endTime) {
                  data[i].date = $filter('date')(data[i].startTime, 'dd');
                  data[i].month = parseInt($filter('date')(data[i].startTime, 'yyyyMM'));
                  data[i].monthDescription = $filter('date')(data[i].startTime, 'MMMM');
                  if(newArray[data[i].month]){
                      newArray[data[i].month].push(data[i]);
                  } else {
                      newArray[data[i].month] = [data[i]];
                  }
                }
            };

            if(Object.keys(newArray).length < 1){
              $scope.events = null;
            } else {
              $scope.events = newArray;
              console.log(newArray)
            }

        }, function(resp){
          $scope.events = null;
          console.log('Events Http request error: ')
        });
    };

    $scope.onEventSelected = function (event) {
        //$scope.event = event;
        $scope.eventViewDate = $filter('date')(event, 'dd MMMM');
        $scope.eventViewData = event;
            $ionicModal.fromTemplateUrl('modules/events/EventsDetailView.html', {
            scope: $scope,
            animation: 'slide-in-up'
            }).then(function(modal) {
                console.log(modal);
                $scope.eventViewModal = modal;
                 $scope.eventViewModal.show();
            });

    };
    $scope.closeEventViewModal = function() {
        $scope.eventViewModal.hide();
    };

    $scope.onTimeSelected = function (selectedTime) {
        // console.log('Selected time: ' + selectedTime);
    };

    $rootScope.scrollHeight = window.innerHeight-390;

})
