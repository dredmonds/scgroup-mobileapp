angular.module('starter.controllers')

.controller('DirectoryController', function(DirectoryService, $ionicLoading, $scope, $rootScope, $ionicSlideBoxDelegate, $ionicScrollDelegate, $ionicModal, $timeout) {

	var vm = this;

	vm.getPeople = getPeople;
	vm.scrollTop = scrollTop;

	vm.people = {};


  $scope.$on( "$ionicView.beforeEnter", function( scopes, states ) {
  	vm.getPeople();
  	vm.getTimeStamp = Date.now();
  });


	function getPeople () {
		$ionicLoading.show();
	    DirectoryService.getPeople()
	      .then(function(success){
	      	vm.people = success.data;
	      	$ionicLoading.hide();
	      }, function(error){
	        console.log(error);
	        $ionicLoading.show();
	      })

	}

	function scrollTop () {
		$ionicScrollDelegate.scrollTop(true);
	}

  $scope.generateAvatarColor = function(fullName) {

		if(fullName){
			var numColors = 11;
	    var hash = 0;
	    if(fullName.length == 0) return hash;
	    for(i = 0; i < fullName.length; i++){
	        char = fullName.charCodeAt(i);
	        hash = ((hash<<5)-hash)+char;
	        hash = hash & hash; // Convert to 32bit integer
	    }
	    hash = Math.abs(hash) % numColors;

	    return hash;
		}

  }

})
