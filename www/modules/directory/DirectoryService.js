angular.module('starter.services')
	
.service('DirectoryService', function( $http, $q, $rootScope ) {

	// Return public API.
	return({
	    getPeople: getPeople,
	    getPerson: getPerson,
	    updatePerson: updatePerson,
	});

	function getPeople(id) {
	    var request = $http({
	        method: "GET",
	        url: localStorage["serverURL"]+"directory",
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function getPerson(id) {
	    var request = $http({
	        method: "GET",
	        url: localStorage["serverURL"]+"directory?user="+id,
	        params: {},
	        data: {}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updatePerson(content) {
	    var request = $http({
	        method: "PUT",
	        headers:{
	        	"content-type":"application/json"
	        },
	        url: localStorage["serverURL"]+"user",
	        params: {},
	        data: {info:content}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}


	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});