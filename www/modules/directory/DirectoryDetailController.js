angular.module('starter.controllers')

.controller('DirectoryDetailController', function(DirectoryService, $cordovaCamera, $cordovaFileTransfer, $ionicLoading, $stateParams, $scope, $ionicHistory, $rootScope, $ionicSlideBoxDelegate, $ionicModal, $timeout) {

	var vm = this;
	
	vm.getPerson = getPerson;
	vm.updatePerson = updatePerson;
	vm.person = {};
	vm.openFilePicker = openFilePicker;

	$scope.$on( "$ionicView.beforeEnter", function( scopes, states ) {
		vm.getPerson();
	});

	function getPerson () {
		$ionicLoading.show();
	    DirectoryService.getPerson($stateParams.userId)
	      .then(function(success){
	      	console.log(success.data[0]);
	      	vm.person = success.data[0];
	      	$ionicLoading.hide();
	      }, function(error){
	        console.log(error);
	        $ionicLoading.hide();
	      })

	}

	// function getPosts () {

	//     DirectoryService.getPerson($stateParams.userId)
	//       .then(function(success){
	//       	console.log(success.data[0]);
	//       	vm.person = success.data[0];
	//       }, function(error){
	//         console.log(error);
	//       })

	// }

	function updatePerson () {
		$ionicLoading.show();
	    DirectoryService.updatePerson(vm.person.info)
	      .then(function(success){
	      	console.log(success);
	      	$ionicLoading.show({template:"Updated"});
	      	$timeout(function(){
	      		$ionicLoading.hide();
	      		$ionicHistory.goBack();
	      	},1000);
	      }, function(error){
	        console.log(error);
	        $ionicLoading.hide();
	      })
	}

	function openFilePicker(person) {
		if(person._id != $rootScope.currentUser._id) {
			return
		}
		var options = {
		destinationType: Camera.DestinationType.FILE_URI,
		sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
		mediaType: Camera.MediaType.ALLMEDIA,
		correctOrientation: true,
		quality: 75,
		// targetHeight: 100,
		// targetWidth: 100
		};

		$cordovaCamera.getPicture(options).then(function(imageUri) {

		  // Do something with image
		  var string = imageUri.toLowerCase();

		  if(!string.match(/\.(jpg|jpeg|png|gif|JPG|JPEG|PNG|GIF)$/)){
		    alert("You cannot upload a video")
		  } else {
		  	uploadFile(imageUri);
		  }

		}, function(err) {

		  console.debug("Unable to obtain picture: " + err, "app");
		  alert(error);

		});

	}

    function uploadFile (imageUri) {
    	$ionicLoading.show({"template":"Uploading"});
      	var token = localStorage["token"].replace(/['"]+/g, '');
        var options = {
          data: {
            file: imageUri
          }, 
          headers: {
            token: token, 
            code:localStorage["orgId"]
          }
        }

        $cordovaFileTransfer.upload(localStorage["serverURL"] + 'avatar', imageUri, options)
          .then(function(result) {
          	// vm.person.avatar = result.response;
          	vm.person.avatar = null;
          	getPerson();
          }, function(error){
            console.log(error);
            alert(error);
            $ionicLoading.hide();
          }, function (progress) {
            $ionicLoading.show({template:"Uploading avatar "+((progress.loaded / progress.total) * 100).toFixed(0)+"%"});
          });

    }

    vm.getTimeStamp = function () {
    	return Date.now();
    }

})