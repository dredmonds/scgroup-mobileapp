angular.module('starter.controllers')

.controller('LibraryController', function(LibraryService, $scope, $rootScope, $state, $ionicSlideBoxDelegate, $ionicModal, $timeout, $http, $stateParams, $ionicTabsDelegate, $ionicHistory) {

    // Getting module options from config file
    for (var i = 0; i < $scope.config.features.length; i++) {
        if($scope.config.features[i].index == $state.params.featureIndex){
            $scope.feature = $scope.config.features[i];
        }
    }

    var vm = this;
    vm.feature = $scope.config.features[$stateParams.featureIndex];
    vm.categories = [];
    vm.refreshLibrary = refreshLibrary;
    //Get data
    var items = [];

    LibraryService.getDocuments()
        .then(function(success){
            items = success.data;
            var master = {};
                for (x in items) {
                    if (master[ items[x].category ] == undefined) {
                        master[ items[x].category ] = {
                            name: items[x].category,
                            values: []
                        };
                    }
                    master[ items[x].category ].values.push( items[x] );
                }

            var ary = [];
            angular.forEach(master, function (val, key) {
                ary.push(val);
            });
            vm.categories = ary;
        }, function(error){
          console.log(error);
        })


    function refreshLibrary () {
        LibraryService.getDocuments()
        .then(function(success){
            items = success.data;
            var master = {};
                for (x in items) {
                    if (master[ items[x].category ] == undefined) {
                        master[ items[x].category ] = {
                            name: items[x].category,
                            values: []
                        };
                    }
                    master[ items[x].category ].values.push( items[x] );
                }
                var ary = [];
                angular.forEach(master, function (val, key) {
                    ary.push(val);
                });
                vm.categories = ary;
        }, function(error){
          console.log(error);
        }).finally(function() {
            $scope.$broadcast('scroll.refreshComplete');
        });
    }


})
