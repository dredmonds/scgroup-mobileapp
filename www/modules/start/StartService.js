angular.module('starter.services', [])

.service('StartService', function( $http, $q, $rootScope ) {

	// Return public API.
	return({
	    getConfigFile: getConfig,
	    getServerURL:getServerURL
	});


	function getServerURL(orgId) {
	    var request = $http({
	        method: "get",
	        url: "https://admin.staffconnectapp.com/api/"+orgId,
		});
	    return( request.then( handleSuccess, handleError ) );
	}



	function getConfig(orgId) {
	    var request = $http({
	        method: "get",
	        url: localStorage["serverURL"]+"config/"+orgId,
		});
	    return( request.then( handleSuccess, handleError ) );
	}


	function handleError( response ) {

	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }

	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
		// response.data.legacy = JSON.stringify(config);
	    return( response );
	}

});
