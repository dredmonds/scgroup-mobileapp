{
	"global": {
		"info": {
			"id": "1234-5678",
			"name": "SC-Team"
		},
		"theme": {
			"header": {
				"headerColor": "red",
				"fontFamily": ""
			},
			"linkColor": "red",
			"fontFamily": "",
			"popup": {
				"title":"yellow",
				"subtitle":"blue",
				"positive": "green",
				"negative": "red"
			},
			"directory": {
				"icon": {
					"backgroundColor": "#2d3642",
					"iconColor": "white"
				}
			},
			"settings": {
				"icon": {
					"backgroundColor": "#eaebec",
					"iconColor": "#2d3642"
				}
			}
		},
		"server": {
			"host": "https://eu-west.staffconnectapp.com/api/"
		},
		"googleAnalytics": {
			"trackingId": "UA-85586344-2",
                        "viewId":"ga:132257789"
		},
		"legalDocuments": {
			"termsconditions": "http://techcrunch.com",
			"privacyPolicy": "http://engadget.com"
		}
	},
	"signin": {
		"backgroundImage": "http://ota.staffconnectapp.com/miscellaneous/images/login-bg.png",
		"header": {
			"statusBar": {
				"backgroundColor": "black",
				"textColor": "white"
			},
			"headerBar": {
				"backgroundColor": "red",
				"textColor": "black",
				"imageUrl": ""
			}
		},
		"form": {
			"username": {
				"type": "text",
				"label": {
					"text": "Your email",
					"color": "black"
				}
			},
			"password": {
				"label": {
					"text": "Your password",
					"color": "black"
				}
			},
			"submit": {
				"text": "LOGIN",
				"textColor": "white",
				"backgroundColor": "#ff5d25"
			}
		},
		"otherTextColor": "stable"
	},
	"signup": {
		"header": {
			"statusBar": {
				"backgroundColor": "",
				"textColor": ""
			},
			"headerBar": {
				"backgroundColor": "",
				"textColor": "",
				"imageUrl": ""
			}
		},
		"form": {
			"acceptedDomains": ["staffconnectapp.com"],
			"submit": {
				"text": "Sign Up",
				"textColor": "white",
				"backgroundColor": "#ff5d25"
			}
		}
	},
	"homescreen": {
		"header": {
			"statusBar": {
				"backgroundColor": "",
				"textColor": ""
			},
			"headerBar": {
				"backgroundColor": "white",
				"textColor": "",
				"imageUrl": "http://ota.staffconnectapp.com/miscellaneous/images/sc-logo.png"
			}
		},
		"panels": [{
			"background": {
				"imageUrl": "http://ota.staffconnectapp.com/miscellaneous/images/panel-bg.png",
				"overlay": true
			},
			"borderColor": "#ff5d25",
			"overlay": {
				"background": {
					"imageUrl": "http://ota.staffconnectapp.com/miscellaneous/images/panel-overlay.png"
				}
			},
			"header": {
				"position": "top-left",
				"text": "Communities",
				"button": {
					"icon": "ion-ios-chatbubble-outline",
					"text": "Join the conversation",
					"backgroundColor": "#ff5d25",
					"textColor": "white"
				}
			},
			"logo": {
				"position": "bottom-right",
				"background": {
					"imageUrl": ""
				}
			},
			"link": 0
		}, {
			"background": {
				"imageUrl": "http://ota.staffconnectapp.com/miscellaneous/images/panel2-bg.png",
				"overlay": true
			},
			"borderColor": "white",
			"overlay": {
				"background": {
					"imageUrl": "http://ota.staffconnectapp.com/miscellaneous/images/panel-overlay.png"
				}
			},
			"header": {
				"position": "top-left",
				"text": "Noticeboard",
				"button": {
					"icon": "ion-ios-paperplane-outline",
					"text": "Read the latest",
					"backgroundColor": "white",
					"textColor": "#ff5d25"
				}
			},
			"logo": {
				"position": "bottom-right",
				"background": {
					"imageUrl": ""
				}
			},
			"link": 1
		}],
		"tickertape": {
			"marquee": true,
			"name": {
				"text": "News",
				"textColor": "white",
				"backgroundColor": "#ff5d25"
			},
			"text": "Welcome to the New World Order (StaffConnect V2).",
			"backgroundColor": "white",
			"textColor": "#2d3642",
			"speed": 15
		},
		"navslider": {
			"background": {
				"image": "",
				"color": "#2d3642"
			}
		}
	},
	"features": [{
		"index": "0",
		"module": "community",
		"moduleId": "1",
		"name": "Communities",
		"navigation": {
			"visible": false,
			"icon": "ion-ios-paper-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "#2d3642",
				"backgroundColor": "white"
			}
		},
		"options": {
			"theme": {
				"like": {
					"iconColor": "#ff5d25"
				},
				"comment": {
					"textColor": "white",
					"backgroundColor": "#2d3642"
				},
				"filter": {
					"textColor": "#ff5d25",
					"backgroundColor": "#ff5d25"
				},
				"newContentIcon":{
					"textColor": "white",
					"backgroundColor": "#2d3642"
				}
			}
		}
	}, {
		"index": "1",
		"module": "community",
		"moduleId": "2",
		"name": "Noticeboard",
		"navigation": {
			"visible": false,
			"icon": "ion-ios-paper-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "#2d3642",
				"backgroundColor": "white"
			}
		},
		"options": {
			"theme": {
				"like": {
					"textColor": "#ff5d25",
					"backgroundColor": "black"
				},
				"comment": {
					"textColor": "white",
					"backgroundColor": "#2d3642"
				},
				"filter": {
					"textColor": "black",
					"backgroundColor": "black"
				},
				"newContentIcon":{
					"textColor": "white",
					"backgroundColor": "#2d3642"
				}
			}
		}
	}, {
		"index": "2",
		"module": "rssfeed",
		"name": "Newsletter",
		"navigation": {
			"visible": true,
			"icon": "ion-ios-paper-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "#2d3642",
				"backgroundColor": "white"
			}
		},
		"options": {
			"intro": {
				"icon": "ion-ios-paper-outline dark",
				"text": "Weekly updates from the StaffConnect team."
			},
			"url": "http://www.staffconnectapp.com/feed/"
		}
	}, {
		"index": "3",
		"module": "library",
		"name": "Library",
		"navigation": {
			"visible": true,
			"icon": "ion-ios-copy-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "#2d3642",
				"backgroundColor": "white"
			}
		},
		"options": {
			"intro": {
				"icon": "ion-ios-copy-outline dark",
				"text": "Welcome to the resource library. Here you can find useful documents and reading materials"
			},
			"categoryBackgroundColor": "stable-bg"
		}
	}, {
		"index": "4",
		"module": "events",
		"name": "Events",
		"navigation": {
			"visible": true,
			"icon": "ion-ios-calendar-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "#2d3642",
				"backgroundColor": "white"
			}
		},
		"options": {}
	}, {
		"index": "5",
		"module": "survey",
		"name": "Survey",
		"navigation": {
			"visible": true,
			"icon": "ion-ios-compose-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "#2d3642",
				"backgroundColor": "white"
			}
		},
		"options": {
			"intro": {
				"icon": "ion-ios-compose-outline dark",
				"text": "Take part in our survey. We appreciate your feedback."
			},
			"theme":{
				"listDivider":{
					"backgroundColor":"#2d3642",
					"textColor":"white"
				}
			},
			"messages": {
				"noLiveSurvey": "No surveys currently available",
				"liveSurveys": "Surveys",
				"completedSurveys": "Completed Surveys"
			}
		}
	}, {
		"index": "6",
		"module": "quiz",
		"name": "Quiz",
		"navigation": {
			"visible": true,
			"icon": "ion-ios-lightbulb-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "#2d3642",
				"backgroundColor": "white"
			}
		},
		"options": {
			"intro": {
				"icon": "ion-document-text dark",
				"text": "Take part in our monthly quiz. Answer as quickly as you can, the quiz is timed!"
			},
			"theme":{
				"listDivider":{
					"backgroundColor":"#2d3642",
					"textColor":"white"
				}
			},
			"messages": {
				"noLiveQuiz": "There are no available quizes.",
				"takeQuiz": "Take a Quiz",
				"leaderboard": "Leaderboards"
			}
		}
	}]
}
