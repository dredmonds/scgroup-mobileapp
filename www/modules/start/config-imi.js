var config = {
	"global": {
		"info": {
			"id": "2345-6789",
			"name": "IMI Staff"
		},
		"theme": {
			"header": {
				"headerColor": "red",
				"fontFamily": ""
			},
			"linkColor": "red",
			"fontFamily": "",
			"popup": {
				"title":"orange",
				"subtitle":"blue",
				"positive": "green",
				"negative": "red"
			},
			"directory": {
				"icon": {
					"backgroundColor": "red",
					"iconColor": "white"
				}
			},
			"settings": {
				"icon": {
					"backgroundColor": "red",
					"iconColor": "white"
				}
			}
		},
		"server": {
			"host": "https://frankfurt.staffconnectapp.com/api/"
		},
		"googleAnalytics": {
			"trackingId": "UA-36500857-11"
		},
		"legalDocuments": {
			"termsconditions": "http://techcrunch.com",
			"privacyPolicy": "http://engadget.com"
		}
	},
	"signin": {
		"backgroundImage": "http://ota.staffconnectapp.com/miscellaneous/v2configs/imi/login-bg.png",
		"header": {
			"statusBar": {
				"backgroundColor": "black",
				"textColor": "white"
			},
			"headerBar": {
				"backgroundColor": "red",
				"textColor": "light",
				"imageUrl": ""
			}
		},
		"form": {
			"username": {
				"type": "text",
				"label": {
					"text": "Your email",
					"color": "light"
				}
			},
			"password": {
				"label": {
					"text": "Your password",
					"color": "light"
				}
			},
			"submit": {
				"text": "LOGIN",
				"textColor": "white",
				"backgroundColor": "green"
			}
		},
		"otherTextColor": "stable"
	},
	"signup": {
		"header": {
			"statusBar": {
				"backgroundColor": "",
				"textColor": ""
			},
			"headerBar": {
				"backgroundColor": "",
				"textColor": "",
				"imageUrl": ""
			}
		},
		"form": {
			"acceptedDomains": ["staffconnectapp.com", "gmail.com"],
			"submit": {
				"text": "Sign Up",
				"textColor": "white",
				"backgroundColor": "green"
			}
		}
	},
	"homescreen": {
		"header": {
			"statusBar": {
				"backgroundColor": "",
				"textColor": ""
			},
			"headerBar": {
				"backgroundColor": "white",
				"textColor": "",
				"imageUrl": "http://ota.staffconnectapp.com/miscellaneous/v2configs/imi/imi-logo.png"
			}
		},
		"panels": [{
			"background": {
				"imageUrl": "http://ota.staffconnectapp.com/miscellaneous/v2configs/imi/slider1.png",
				"overlay": true
			},
			"borderColor": "red",
			"overlay": {
				"background": {
					"imageUrl": "http://ota.staffconnectapp.com/miscellaneous/images/panel-overlay.png"
				}
			},
			"header": {
				"position": "top-left",
				"text": "Leadership messages",
				"button": {
					"icon": "ion-ios-paper",
					"text": "Read the latest",
					"backgroundColor": "red",
					"textColor": "white"
				}
			},
			"logo": {
				"position": "bottom-right",
				"background": {
					"imageUrl": ""
				}
			},
			"link": 0
		}, {
			"background": {
				"imageUrl": "http://ota.staffconnectapp.com/miscellaneous/v2configs/imi/slider2.png",
				"overlay": true
			},
			"borderColor": "black",
			"overlay": {
				"background": {
					"imageUrl": "http://ota.staffconnectapp.com/miscellaneous/images/panel-overlay.png"
				}
			},
			"header": {
				"position": "top-left",
				"text": "Community hub",
				"button": {
					"icon": "ion-ios-paperplane-outline",
					"text": "Get involved",
					"backgroundColor": "yellow",
					"textColor": "black"
				}
			},
			"logo": {
				"position": "bottom-right",
				"background": {
					"imageUrl": ""
				}
			},
			"link": 1
		}],
		"tickertape": {
			"marquee": true,
			"name": {
				"text": "News",
				"textColor": "red",
				"backgroundColor": "blue"
			},
			"text": "Please complete the Final Survey in-app. Your feedback is very valuable to us !!",
			"backgroundColor": "red",
			"textColor": "black",
			"speed": 15
		},
		"navslider": {
			"background": {
				"image": "",
				"color": "purple"
			}
		}
	},
	"features": [{
		"index": "0",
		"module": "community",
		"moduleId": "1",
		"name": "Leadership Messages",
		"navigation": {
			"visible": false,
			"icon": "ion-ios-paper-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "white",
				"backgroundColor": "white"
			}
		},
		"options": {
			"theme": {
				"like": {
					"textColor": "white",
					"backgroundColor": "black"
				},
				"comment": {
					"textColor": "white",
					"backgroundColor": "black"
				},
				"filter": {
					"textColor": "black",
					"backgroundColor": "black"
				},
				"newContentIcon":{
					"textColor": "white",
					"backgroundColor": "black"
				}
			}
		}
	}, {
		"index": "1",
		"module": "community",
		"moduleId": "2",
		"name": "Community hub",
		"navigation": {
			"visible": false,
			"icon": "ion-ios-paper-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "white",
				"backgroundColor": "white"
			}
		},
		"options": {
			"theme": {
				"like": {
					"textColor": "white",
					"backgroundColor": "black"
				},
				"comment": {
					"textColor": "white",
					"backgroundColor": "black"
				},
				"filter": {
					"textColor": "black",
					"backgroundColor": "black"
				},
				"newContentIcon":{
					"textColor": "white",
					"backgroundColor": "black"
				}
			}
		}
	}, {
		"index": "2",
		"module": "community",
		"moduleId": "3",
		"name": "Products",
		"navigation": {
			"visible": true,
			"icon": "ion-ios-paper-outline",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "white",
				"backgroundColor": "white"
			}
		},
		"options": {
			"intro": {
				"icon": "ion-ios-paper-outline dark",
				"text": "Weekly updates from the Product team."
			},
			"theme": {
				"like": {
					"textColor": "white",
					"backgroundColor": "black"
				},
				"comment": {
					"textColor": "white",
					"backgroundColor": "black"
				},
				"filter": {
					"textColor": "black",
					"backgroundColor": "black"
				},
				"newContentIcon":{
					"textColor": "white",
					"backgroundColor": "black"
				}
			}
		}
	}, {
		"index": "3",
		"module": "library",
		"name": "Library",
		"navigation": {
			"visible": true,
			"icon": "ion-ios-copy-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "white",
				"backgroundColor": "white"
			}
		},
		"options": {
			"intro": {
				"icon": "ion-ios-copy-outline dark",
				"text": "Welcome to the resource library. Here you can find useful documents and reading materials"
			},
			"categoryBackgroundColor": "stable-bg"
		}
	}, {
		"index": "4",
		"module": "events",
		"name": "Events",
		"navigation": {
			"visible": true,
			"icon": "ion-ios-calendar-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "white",
				"backgroundColor": "white"
			}
		},
		"options": {}
	}, {
		"index": "5",
		"module": "survey",
		"name": "Survey",
		"navigation": {
			"visible": true,
			"icon": "ion-ios-compose-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "white",
				"backgroundColor": "white"
			}
		},
		"options": {
			"intro": {
				"icon": "ion-ios-compose-outline dark",
				"text": "Take part in our survey. We appreciate your feedback."
			},
			"surveyBar": "energized-bg light",
			"messages": {
				"noLiveSurvey": "No surveys currently available",
				"liveSurveys": "Live Surveys",
				"completedSurveys": "Completed Surveys"
			}
		}
	}, {
		"index": "6",
		"module": "rssfeed",
		"name": "Newsletter",
		"navigation": {
			"visible": true,
			"icon": "ion-ios-paper-outline dark",
			"iconOuter": {
				"backgroundColor": "white"
			},
			"iconInner": {
				"borderColor": "white",
				"backgroundColor": "white"
			}
		},
		"options": {
			"intro": {
				"icon": "ion-ios-paper-outline dark",
				"text": "Weekly updates from the StaffConnect team."
			},
			"url": "http://www.staffconnectapp.com/feed/"
		}
	}]
}