angular.module('starter.controllers')

.controller('StartController', function(StartService, $scope, $cordovaNativeStorage, $rootScope, $ionicModal, $timeout, $ionicLoading, $state, $ionicPopup, $rootScope, $ionicHistory) {

	// With the new view caching in Ionic, Controllers are only called
	// when they are recreated or on app start, instead of every page change.
	// To listen for when this page is active (for example, to refresh data),
	// listen for the $ionicView.enter event:
	$timeout(function() {
    	$rootScope.isSplashScreenHide = true;
        navigator.splashscreen.hide();
    }, 1500);

	$scope.$on('$ionicView.enter', function(e) {
		$ionicHistory.clearHistory();

		delete localStorage.config;
		delete localStorage.localStorage;
		delete localStorage.orgId;
		delete localStorage.serverURL;

		$cordovaNativeStorage.remove("localStorage");
	});

	$scope.retrieveServer = function (orgId) {

		$ionicLoading.show();
		retrieveServer(orgId);
	}

	function retrieveServer (orgId) {

        StartService.getServerURL(orgId)
            .then(function(success){

            	localStorage["serverURL"] = success.data;
            	retrieveConfig(orgId);

            }, function( errorMessage ) {

				$ionicLoading.hide();
		        $ionicPopup.alert({
		          title: "Not found",
		          template: '<center>Please enter a valid organisation id.</center>'
		        });

            });

	}

	function retrieveConfig (orgId) {

        StartService.getConfigFile(orgId)
            .then(function(success){

            	success.data.legacy = JSON.parse( success.data.legacy );
            	localStorage["orgId"] = orgId;
							localStorage["databaseName"] = success.data.database.name;
							localStorage["config"] = JSON.stringify( success.data.legacy );
							$rootScope.config = success.data.legacy;
							$ionicLoading.hide();
							$state.go("app.signin");

            }, function( errorMessage ) {

				$ionicLoading.hide();
		        $ionicPopup.alert({
		          title: "Not found",
		          template: '<center>Please enter a valid organisation id.</center>'
		        });

            });

	}

})
