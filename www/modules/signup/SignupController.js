angular.module('starter.controllers')

.controller('SignupController', function(StartService, SignupService, $scope, $rootScope, $ionicModal, $timeout, $ionicPopup, $state, $stateParams) {

  $scope.user = {};

  if(!$rootScope.isSplashScreenHide) {
    $timeout(function() {
        $rootScope.isSplashScreenHide = true;
          navigator.splashscreen.hide();
      }, 1500);
  }

  if($state.current.name == 'app.signup-nondesk') {
    $scope.userObject = JSON.parse($stateParams.data);
  }

  $scope.verifyNondesk = function (user) {
    console.log(user);
      var missingFields = [];
      if (!user || !user.data1) {
        missingFields.push("<br>"+$rootScope.config.signup.methods.nondesk.label1);
      }
      if (!user || !user.data2) {
        missingFields.push("<br>"+$rootScope.config.signup.methods.nondesk.label2);
      }
      if(missingFields.length > 0){
          $ionicPopup.alert({
            title: 'Registration Failed',
            template: '<center>Please ensure the following fields have been entered correctly:<br>'+missingFields.toString().replace(/,/g , " ")+'</center>'
          });
      } else {
          SignupService.verify(user)
            .then(function(success){
                $scope.user = {};
                $state.go("app.signup-nondesk", {data:JSON.stringify(success.data)});
            }, function( errorMessage ) {
                console.warn( errorMessage );
                $ionicPopup.alert({
                  title: 'Registration Failed',
                  template: '<center>'+errorMessage.data+'</center>'
                });
            });
      }

  }

  $scope.createUser = function (user) {

      var missingFields = [];
      if ( !user || !validateEmail(user.username) ){
        missingFields.push("<br>Email address");
      }
      if (!user || !user.first_name) {
        missingFields.push("<br>First name");
      }
      if (!user || !user.last_name) {
        missingFields.push("<br>Last name");
      }
      if (!user || !user.password) {
        missingFields.push("<br>Password");
      }
      if (user && user.password && !user.repeatPassword) {
        missingFields.push("<br>Repeat password");
      }
      if (user && user.repeatPassword && user.password && user.password != user.repeatPassword) {
        missingFields.push("<br>Passwords don't match");
      }
      if(missingFields.length > 0){
          $ionicPopup.alert({
            title: 'Registration Failed',
            template: '<center>Please ensure the following fields have been entered correctly:<br>'+missingFields.toString().replace(/,/g , " ")+'</center>'
          });
      } else {
          user.info = {"created_method":"app_domain"};
          SignupService.create(user)
            .then(function(success){
                  $ionicPopup.alert({
                    title: 'Verify account',
                    template: '<center>Thank you. We have sent you an email containing an activation link. Please use this link to verify your account before logging in.</center>'
                  });
                  $scope.user = {};
                  $state.go("app.signin");

            }, function( errorMessage ) {
                  console.warn( errorMessage );
                  $ionicPopup.alert({
                    title: 'Registration Failed',
                    template: '<center>'+errorMessage.data+'</center>'
                  });
            });
      }

  }

  $scope.createNondeskUser = function (user) {

      var missingFields = [];
      if ( !user || !validateEmail(user.username, true) ){
        missingFields.push("<br>Email address");
      }
      if (!user || !user.password) {
        missingFields.push("<br>Password");
      }
      if (user && user.password && !user.repeatPassword) {
        missingFields.push("<br>Repeat password");
      }
      if (user && user.repeatPassword && user.password && user.password != user.repeatPassword) {
        missingFields.push("<br>Passwords don't match");
      }
      if(missingFields.length > 0){
          $ionicPopup.alert({
            title: 'Registration Failed',
            template: '<center>Please ensure the following fields have been entered correctly:<br>'+missingFields.toString().replace(/,/g , " ")+'</center>'
          });
      } else {
          var userToSubmit = angular.copy(user);
          userToSubmit.location = user.location._id;
          userToSubmit.department = user.department._id;
          userToSubmit.info = {"created_method":"app_nondesk"};
          SignupService.create(userToSubmit)
            .then(function(success){
              deleteInvite(user);
              $ionicPopup.alert({
                title: 'Verify account',
                template: '<center>Thank you. We have sent you an email containing an activation link. Please use this link to verify your account before logging in.</center>'
              });
              $scope.user = {};
              $state.go("app.signin");

              }, function( errorMessage ) {
                  console.warn( errorMessage );
              $ionicPopup.alert({
                title: 'Registration Failed',
                template: '<center>'+errorMessage.data+'</center>'
              });
            });
      }

  }

  function deleteInvite(user) {
    SignupService.deleteInvite(user)
      .then(function(success){
          console.log("user invite data deleted")
        }, function( errorMessage ) {
          console.warn( "user invite data NOT DELETED" );
      });
  }

  $scope.goToTerms = function(){
    var options = {
      location: 'no',
      clearcache: 'yes',
      toolbar: 'yes',
      EnableViewPortScale:'yes',
      closebuttoncaption: 'Back',
      toolbarposition:'top'
    };
    var url = Settings.server() + 'public/termsOfUsage.pdf';
    if(ionic.Platform.isAndroid()){
      $cordovaInAppBrowser.open('https://docs.google.com/viewer?url=' + url + '&embedded=true', '_blank', options);
    }
    if(ionic.Platform.isIOS()){
      $cordovaInAppBrowser.open(url, '_blank', options);
    }
  }

  function validateEmail(email, skip) {

      var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if(skip != true && $rootScope.config.signup.methods.companydomain.acceptedDomains && re.test(email) == true){
        var emailDomain = email.split("@").pop();
        for (var i = 0; i < $rootScope.config.signup.methods.companydomain.acceptedDomains.length; i++) {
          if($rootScope.config.signup.methods.companydomain.acceptedDomains[i] == emailDomain || "staffconnectapp.com" == emailDomain){
            return true;
          }
        }
        return false;

      } else {
        return re.test(email);
      }


  }

})
