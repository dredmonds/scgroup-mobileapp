angular.module('starter.services')

.service('SignupService', function( $rootScope, $http, $q ) {

	// Return public API.
	return({
	    authenticate: authenticateUser,
	    create: createUser,
	    verify: verifyUser,
	    deleteInvite: deleteInvite,
	    // get: getUser,
	    update: updateUser,
	    getCurrentUser: getCurrentUser,
	    // delete: deleteUser,
	    resetPassword:resetPassword,
	    signOut:signOutUser,
	});

	function createUser(userObject) {
		delete userObject._id;
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "post",
	        url: host+"auth/register",
	        data: userObject
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function verifyUser(userObject) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "post",
	        url: host+"auth2",
	        data: userObject
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function deleteInvite(userObject) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "put",
	        url: host+"auth2",
	        data: {data1:userObject.data1, data2:userObject.data2}
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function authenticateUser(userObject) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "post",
	        url: host+"auth/login",
	        data: userObject
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function signOutUser() {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "post",
	        url: host+"auth/logout",
	    });
			delete localStorage.communities;
			delete localStorage.currentUser;
			delete localStorage.token;
	    return( request.then( handleSuccess, handleError ) );
	}

	function resetPassword(userObject) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "post",
	        url: host+"auth/forgotpass",
	        data: userObject
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	function updateUser(id) {
		var host = localStorage["serverURL"];
	    var request = $http({
	        method: "get",
	        url: host+"auth/login",
	        params: {
	            action: "get"
	        },
	        data: {
	            id: id
	        }
	    });
	    return( request.then( handleSuccess, handleError ) );
	}

	// function getUser(id) {
	//     var request = $http({
	//         method: "get",
	//         url: "api/index.cfm",
	//         params: {
	//             action: "get"
	//         },
	//         data: {
	//             id: id
	//         }
	//     });
	//     return( request.then( handleSuccess, handleError ) );
	// }

	function getCurrentUser(id) {
		var currentUser = JSON.parse(localStorage["currentUser"]);
	    return currentUser;
	}

	// function deleteUser( id ) {
	//     var request = $http({
	//         method: "delete",
	//         url: "api/index.cfm",
	//         params: {
	//             action: "delete"
	//         },
	//         data: {
	//             id: id
	//         }
	//     });
	//     return( request.then( handleSuccess, handleError ) );
	// }

	function handleError( response ) {
	    // The API response from the server should be returned in a
	    // nomralized format. However, if the request was not handled by the
	    // server (or what not handles properly - ex. server error), then we
	    // may have to normalize it on our end, as best we can.
	    if (
	        ! angular.isObject( response ) ||
	        ! response
	        ) {
	        return( $q.reject( "An unknown error occurred." ) );
	    }
	    // Otherwise, use expected error message.
	    return( $q.reject( response ) );
	}

	function handleSuccess( response ) {
	    return( response );
	}


});
