angular.module('starter.controllers')

.controller('SurveyController', function(LibraryService, $scope, $rootScope, $ionicSlideBoxDelegate, $ionicModal, $timeout, $http, $stateParams, $ionicTabsDelegate, $ionicHistory) {
    var vm = this;
    vm.feature = $scope.config.features[$stateParams.featureIndex];
    vm.currentUser = $rootScope.currentUser;
    vm.featureIndex = $stateParams.featureIndex;
	
    $scope.$on('$ionicView.enter', function(e) {
    	getSurveys();
    });

	function getSurveys () {

 		$http({
        	method:"GET",
	        url: localStorage["serverURL"]+'quiz/all',
	        params: {},
	        data: {}
        }).then(function(result) {
        	var surveryAll = result.data;
	        //get survey response
	        $http({
	        	method:"GET",
		        url: localStorage["serverURL"]+ 'quiz/response/'+vm.currentUser.username,
		        params: {},
		        data: {}
	        }).then(function(surveyResponse){
	            var surveyResp = surveyResponse.data;
	            var surveyList = [];
	            var completedSurvey = [];
	            var incompletedSurvey = [];
	            vm.noSurveyAvail = false;
	            vm.liveSurveys = true;
				for(var i in surveryAll){
					if(surveryAll[i].type == 1 && surveryAll[i].published == 1){
						surveyList.push(surveryAll[i]);
					}
				}

				for (var survey = 0; survey < surveyList.length; survey++) {
					incompletedSurvey[survey] = surveyList[survey];
					if(surveyResp != ''){
						for(var response in surveyResp){
							if(surveyList[survey] && surveyList[survey].id == surveyResp[response].quiz){
								completedSurvey.push(surveyList[survey]);
								delete incompletedSurvey[survey];
							} 
						}
					}else{
						vm.incompleteSurvey = incompletedSurvey;
						vm.liveSurveys = true;
					}
				};
	            vm.completedSurvey = completedSurvey;
	            vm.incompleteSurvey = incompletedSurvey;
	            if(vm.completedSurvey.length == surveyList.length){
	            	vm.noSurveyAvail = true;
	            }
	          }, function(error) {
	            	console.log(error);
	          });


	    }, function(error) {
	        console.log(error);
	    });

	}


})
  .controller('SurveyQuestionsController', function ($scope, $rootScope, $http, $stateParams, $state, $ionicPopup, $timeout) {
    var vm = this;
    vm.currentUser = $rootScope.currentUser;

	$http({
		method:"GET",
		url: localStorage["serverURL"] + 'quiz/' + $stateParams.id,
		params: {},
		data: {}
	}).then(function(result) {

        $scope.quizData = result.data;

        $scope.totalQues = $scope.quizData['questions'].length;
        $scope.question_index = 0;
        

        $scope.validateNext = function(a, b, response){

        	if((a==false && b==undefined) || (a==true && b==undefined)){
        		$scope.showErrorMessage();
        	}else if(a==false && b!=undefined){
        		$scope.next();
        	}else if(a==true && b!=undefined){
        		$scope.quiz_submit(response);
        	}
        }


        $scope.fillAnswer = false;
        $scope.showErrorMessage = function(){
         	$scope.fillAnswer = true;
        }
        $scope.next = function(){
	        $scope.fillAnswer = false;
	        if($scope.question_index >= $scope.quizData['questions'].length - 1){
	        	$scope.question_index = 0;
	        }else{
	        	$scope.question_index++;
	        }
        };
        $scope.prev = function(){
            $scope.question_index--;
        };
        $scope.response = {};
        $scope.quiz_submit = function(response) {
	        var submit = []
	        var points = 0;

	        for (i in $scope.quizData.questions) {
	            var data = {};
	            data.question = $scope.quizData.questions[i].id;
	            if (typeof response[ data.question ] == 'object') {
	                data.answer = Object.keys(response[data.question]);
	            } else {
	                data.answer = response[ data.question ];
	      		}
	            submit.push(data);
	        	console.log(submit);
	        }
	        
	        $http({
				method:"POST",
				headers:{
					"content-type":"application/json"
				},
				url: localStorage["serverURL"] + 'quiz/response',
				params: {},
				data: {user: vm.currentUser.username,firstname: vm.currentUser.first_name,lastname: vm.currentUser.last_name,quiz: $stateParams.id,answer: JSON.stringify(submit),points: points,time:0}
			}).then(function(result) {
               	$ionicPopup.alert({
				  title: 'Survey Complete',
				  template: '<span class="surveyPopup">Thank you for your feedback.</span>',
				  buttons: [
				    {
				      text: 'Close',
				      type: 'button-assertive'
				    }
				  ]
				});
            
             $state.go('app.survey', {featureIndex:$stateParams.featureIndex});
			}, function(error) {
			console.log(error);
			})
        }

      }, function(error) {
        console.log(error);
      })

  }).filter('isEmpty', [function() {
  return function(object) {
    return angular.equals({}, object);
  }
}])
