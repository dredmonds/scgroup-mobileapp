// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'starter.directives', 'ngCordova', 'ngCordova.plugins.nativeStorage', 'ui.rCalendar', 'timer', 'angularMoment'])

.run(function($ionicPlatform, $cordovaNativeStorage, StartService, $rootScope, ConnectivityMonitor, $cordovaPushV5, $state, CommunityService, $ionicPopup, $timeout, $window) {


    $rootScope.isFullScreenMode = false;
    $rootScope.isSplashScreenHide = false;

    $ionicPlatform.on('resume', function() {
        if ($rootScope.isFullScreenMode) {
            $timeout(function() {
                if (window.StatusBar) window.StatusBar.hide();
            }, 250);
        }
    });

    $ionicPlatform.ready(function() {


        if (localStorage["config"]) {
            // save localstorage to nativestorage
            if (localStorage["localStorage"]) { delete localStorage["localStorage"]; };
            $cordovaNativeStorage.setItem("localStorage", JSON.stringify(localStorage)).then(function(value) {
            }, function(error) {
                console.log(error);
            });

        } else {

            // check if anything in native storage.
            $cordovaNativeStorage.getItem("localStorage").then(function(value) {

                newLocalStorage = JSON.parse(value);
                for (key in newLocalStorage) {
                    localStorage[key] = newLocalStorage[key];
                }

                if (localStorage["currentUser"]) {
                    $state.go("app.homescreen");
                } else if (localStorage["config"]) {
                    $state.go("app.signin");
                } else {
                    $state.go("app.start");
                }

            }, function(error) {
                console.log(error);
            });

        }

        ConnectivityMonitor.startWatching();

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            // StatusBar.styleLightContent();
            /*StatusBar.backgroundColorByHexString("#ffffff");*/
            StatusBar.styleDefault();
            if(screen) screen.lockOrientation('portrait');
        }

        if ($window.MobileAccessibility) {
            $window.MobileAccessibility.usePreferredTextZoom(false);
        }

        // register iOS push
        var options = {
            android: {
                senderID: "977625339199",
                icon: "ic_stat_notification"
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            },
            windows: {}
        };

        // initialize
        localStorage.registrationId = "";


        if (window.cordova) {

            $cordovaPushV5.initialize(options).then(function(result) {

                console.log(result);
                $cordovaPushV5.setBadgeNumber(0);
                // // start listening for new notifications
                $cordovaPushV5.onNotification();
                // // start listening for errors
                $cordovaPushV5.onError();
                // register to get registrationId
                $cordovaPushV5.register().then(function(registrationId) {

                    // save `registrationId` somewhere;
                    var currentPlatform = ionic.Platform.platform();
                    localStorage.registrationId = registrationId;

                }, function(error) {
                    // alert("ERROR: push registration with platform failed", registrationId);
                    console.log(error);
                })


            });

        }
        // triggered every time notification received
        $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {
            // data.message,
            // data.title,
            // data.count,
            // data.sound,
            // data.image,
            // data.additionalData

            // If message data has a post id
            if (data.additionalData.type == 'comment' || data.additionalData.type == 'like') {

                if ($state.params && $state.params.post) {
                    var post = JSON.parse($state.params.post);
                }

                if (post && post._id == data.additionalData.id) {
                    $rootScope.$broadcast("refreshComments", data.additionalData.id);
                } else if (data.additionalData.foreground) {
                    $ionicPopup.confirm({
                        title: 'View Notification?',
                        template: data.message
                    }).then(function(res) {
                        if (res) getPost(data.additionalData.id);
                    });
                } else {
                    getPost(data.additionalData.id);
                }

            } else if (data.additionalData.type == 'global') {

                $ionicPopup.alert({
                    title: 'Notification',
                    template: data.message
                });

            }

        });

        // triggered every time error occurs
        $rootScope.$on('$cordovaPushV5:errorOcurred', function(event, e) {
            // e.message
            alert(e.message)
        });

        function getPost(id) {
            CommunityService.getPost(id)
                .then(function(success) {

                    success.data[0].author.full_name = success.data[0].author.first_name + ' ' + success.data[0].author.last_name;

                    if (success.data[0].likes.indexOf($rootScope.currentUser._id) > -1) {
                        success.data[0].liked = true;
                    }
                    if (success.data[0].subscribe.indexOf($rootScope.currentUser._id) > -1) {
                        success.data[0].subscribed = true;
                    }

                    var post = JSON.stringify(success.data[0]);

                    $state.go("app.community-detail", { id: success.data[0].community, post: post });

                }, function(error) {
                    console.log(error);
                });
        }

    });


})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {

    $httpProvider.defaults.headers.common = {};
    $httpProvider.defaults.headers.post = {};
    $httpProvider.defaults.headers.put = {};
    $httpProvider.defaults.headers.patch = {};

    $httpProvider.interceptors.push('authInterceptor');

    $ionicConfigProvider.backButton.previousTitleText(false);
    $ionicConfigProvider.backButton.text('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
    $ionicConfigProvider.spinner.icon("lines");
    $ionicConfigProvider.navBar.alignTitle("center");

    $ionicConfigProvider.views.swipeBackEnabled(false);
    $ionicConfigProvider.views.transition('none');

    $stateProvider

        .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'modules/main/MainView.html',
        controller: 'MainController'
    })

    .state('app.start', {
        url: '/start',
        views: {
            'menuContent': {
                templateUrl: 'modules/start/StartView.html',
                controller: 'StartController'
            }
        }
    })

    .state('app.signin', {
        url: '/signin',
        views: {
            'menuContent': {
                templateUrl: 'modules/signin/SigninView.html',
                controller: 'SigninController'
            }
        }
    })

    .state('app.signup', {
        url: '/signup',
        views: {
            'menuContent': {
                templateUrl: 'modules/signup/SignupView.html',
                controller: 'SignupController'
            }
        }
    })

    .state('app.signup-verification', {
        url: '/signup-verification',
        views: {
            'menuContent': {
                templateUrl: 'modules/signup/VerificationView.html',
                controller: 'SignupController'
            }
        }
    })

    .state('app.signup-nondesk', {
        url: '/signup-nondesk/:data',
        views: {
            'menuContent': {
                templateUrl: 'modules/signup/SignupNondeskView.html',
                controller: 'SignupController'
            }
        }
    })

    .state('app.homescreen', {
        url: '/homescreen',
        views: {
            'menuContent': {
                templateUrl: 'modules/homescreen/HomescreenView.html',
                controller: 'HomescreenController'
            }
        }
    })

    .state('app.settings', {
        url: '/settings',
        views: {
            'menuContent': {
                templateUrl: 'modules/settings/SettingsView.html',
                controller: 'SettingsController',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.rssfeed', {
        url: '/:featureIndex/rssfeed',
        views: {
            'menuContent': {
                templateUrl: 'modules/rssfeed/RssfeedView.html',
                controller: 'RssfeedController',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.questions', {
        url: '/:featureIndex/questions',
        views: {
            'menuContent': {
                templateUrl: 'modules/questions/QuestionsView.html',
                controller: 'QuestionsController',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.community', {
        url: '/:featureIndex/community/:search/:person/:personName',
        views: {
            'menuContent': {
                templateUrl: 'modules/community/CommunityView.html',
                controller: 'CommunityController',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.community-detail', {
        url: '/:featureIndex/community-detail/:post',
        views: {
            'menuContent': {
                templateUrl: 'modules/community/CommunityDetailView.html',
                controller: 'CommunityController',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.directory', {
        url: '/directory',
        views: {
            'menuContent': {
                templateUrl: 'modules/directory/DirectoryView.html',
                controller: 'DirectoryController',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.directory-detail', {
        url: '/directory-detail/:userId',
        views: {
            'menuContent': {
                templateUrl: 'modules/directory/DirectoryDetailView.html',
                controller: 'DirectoryDetailController',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.user-edit', {
        url: '/directory-detail/:userId/edit',
        views: {
            'menuContent': {
                templateUrl: 'modules/directory/UserEditView.html',
                controller: 'DirectoryDetailController',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.library', {
            url: '/:featureIndex/library',
            views: {
                'menuContent': {
                    templateUrl: 'modules/library/LibraryView.html',
                    controller: 'LibraryController',
                    controllerAs: 'vm'
                }
            }
        })
        .state('app.survey', {
            url: '/:featureIndex/survey',
            views: {
                'menuContent': {
                    templateUrl: 'modules/survey/SurveyView.html',
                    controller: 'SurveyController',
                    controllerAs: 'vm'
                }
            }
        })
        .state('app.survey-questions', {
            url: '/:featureIndex/survey/questions/:id',
            views: {
                'menuContent': {
                    templateUrl: 'modules/survey/SurveyQuestionsView.html',
                    controller: 'SurveyQuestionsController',
                    controllerAs: 'vm'
                }
            }
        })
        .state('app.events', {
            url: '/:featureIndex/events',
            views: {
                'menuContent': {
                    templateUrl: 'modules/events/EventsView.html',
                    controller: 'EventsController',
                    controllerAs: 'vm'
                }
            }
        }).state('app.quiz', {
            url: '/:featureIndex/quiz',
            views: {
                'menuContent': {
                    templateUrl: 'modules/quiz/QuizView.html',
                    controller: 'QuizController',
                    controllerAs: 'vm'
                }
            }
        })
        .state('app.quiz-questions', {
            url: '/:featureIndex/quiz/questions/:id',
            views: {
                'menuContent': {
                    templateUrl: 'modules/quiz/QuizQuestionsView.html',
                    controller: 'QuizQuestionsController',
                    controllerAs: 'vm'
                }
            }
        })
        .state('app.leaderboard', {
            url: '/:featureIndex/quiz/leaderboard/:id',
            views: {
                'menuContent': {
                    templateUrl: 'modules/quiz/QuizLeaderboardView.html',
                    controller: 'leaderboard',
                    controllerAs: 'vm'
                }
            }
        });


    // if none of the above states are matched, use this as the fallback
    if (localStorage["currentUser"]) {
        $urlRouterProvider.otherwise('/app/homescreen');
    } else if (localStorage["config"]) {
        $urlRouterProvider.otherwise('/app/signin');
    } else {
        $urlRouterProvider.otherwise('/app/start');
    }

})

.factory('authInterceptor', function($q, $rootScope) {
    return {
        request: function(config) {
            var host = localStorage["serverURL"];
            if ($rootScope.config && config.url.includes(host)) {
                config.headers = config.headers || {};
                if (localStorage['token']) {
                    config.headers.token = localStorage["token"].replace(/['"]+/g, '');
                }
                config.headers.code = localStorage["orgId"];
                config.headers["content-type"] = "application/json";
            }
            return config;
        },

        responseError: function(response) {
            if (response.status === 401) {
                $rootScope.$broadcast("signout");
                return $q.reject(response);
            } else {
                return $q.reject(response);
            }
        }
    };
})

.directive('imageonload', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('load', function() {
                scope.$apply(attrs.imageonload)(true);
            });
            element.bind('error', function() {
                scope.$apply(attrs.imageonload)(false);
            });
        }
    };
})

.directive('commentSubmit', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            element.bind('click', function() {
                scope.$apply(function() {
                    scope.isDisabled = true;
                });
            });
        }
    };
})

.filter('toArray', function() {
    return function(obj, addKey) {
        if (!angular.isObject(obj)) return obj;
        if (addKey === false) {
            return Object.keys(obj).map(function(key) {
                return obj[key];
            });
        } else {
            return Object.keys(obj).map(function(key) {
                var value = obj[key];
                return angular.isObject(value) ?
                    Object.defineProperty(value, '$key', { enumerable: false, value: key }) : { $key: key, $value: value };
            });
        }
    };
})

.factory('ConnectivityMonitor', function($rootScope, $cordovaNetwork) {

    return {
        isOnline: function() {
            if (ionic.Platform.isWebView()) {
                return $cordovaNetwork.isOnline();
            } else {
                return navigator.onLine;
            }
        },
        isOffline: function() {
            if (ionic.Platform.isWebView()) {
                return !$cordovaNetwork.isOnline();
            } else {
                return !navigator.onLine;
            }
        },
        startWatching: function() {
            $rootScope.isOnline = true;
            if (ionic.Platform.isWebView()) {
                $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
                    console.log("went online");
                    $rootScope.isOnline = true;
                });

                $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
                    console.log("went offline");
                    $rootScope.isOnline = false;
                });

            } else {

                window.addEventListener("online", function(e) {
                    console.log("went online");
                    $rootScope.isOnline = true;
                }, false);

                window.addEventListener("offline", function(e) {
                    console.log("went offline");
                    $rootScope.isOnline = false;
                }, false);
            }
        }
    }
});
