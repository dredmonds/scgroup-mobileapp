#!/bin/sh

if [[ "$TRAVIS_BRANCH" == "development" ]]; then
    fastlane beta
    exit $?
fi
